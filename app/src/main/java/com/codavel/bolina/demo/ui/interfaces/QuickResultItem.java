/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.interfaces;

import com.codavel.bolina.demo.modal.TestResult;

/**
 * Item entry to present results in UI
 */
public class QuickResultItem implements ListItem {
    /**
     * TestResult with results
     */
    public TestResult testData;
    /**
     * Type of ListItem
     */
    public int getTypes;

    /**
     * QuickResultItem constructor
     *
     * @param data     TestResult
     * @param getTypes ListItem type
     */
    public QuickResultItem(TestResult data, int getTypes) {
        this.testData = data;
        this.getTypes = getTypes;
    }

    /**
     * Getter for type of ListItem
     *
     * @return type of ListItem
     */
    @Override
    public int getType() {
        return TYPE_QR_ITEM;
    }
}
