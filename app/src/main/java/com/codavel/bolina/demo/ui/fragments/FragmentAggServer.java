/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.codavel.bolina.demo.R;
import com.codavel.bolina.demo.modal.TestResult;
import com.codavel.bolina.demo.persistence.sql.DatabaseHandler;
import com.codavel.bolina.demo.ui.adapters.AdapterResultEnum;
import com.codavel.bolina.demo.ui.adapters.ResultAdapter;
import com.codavel.bolina.demo.ui.interfaces.ListItem;
import com.codavel.bolina.demo.ui.interfaces.MyLinkedMap;
import com.codavel.bolina.demo.ui.interfaces.QuickResultHeader;
import com.codavel.bolina.demo.ui.interfaces.QuickResultItem;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment to manage the UI Server Aggregation Tab.
 */
public class FragmentAggServer extends Fragment {
    /**
     * Application Context
     */
    private Context mContext;
    /**
     * List with results aggregated by protocol and network
     */
    private MyLinkedMap<String, ListItem> itemList;

    @BindView(R.id.recyclerVTestHistory)
    RecyclerView recyclerVTestHistory;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.rlNoData)
    RelativeLayout rlNoData;

    /**
     * On create fetches the List of results to be presented
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        fetchPersistedData();
    }

    /**
     * Inflates the layout on the View
     *
     * @param inflater           Layout inflater
     * @param container          ViewGroup container
     * @param savedInstanceState Bundle with saved instance
     * @return View created
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        fetchPersistedData();
        return inflater.inflate(R.layout.fragment_agg_server, container, false);
    }

    /**
     * Set a hint to the system about whether this fragment's UI is currently visible to the user.
     *
     * @param isVisibleToUser boolean indicating if fragment is visible to the user
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            getFragmentManager().beginTransaction().detach(this).attach(this).commit();
        }
    }

    /**
     * Defines the listener for the refresh layout and calls Adapter setup case the list of results
     * is not empty.
     *
     * @param view               View created
     * @param savedInstanceState Bundle with saved instance
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();
        ButterKnife.bind(this, view);

        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                fetchPersistedData();
                if (itemList.size() > 0) {
                    setAdapter();
                    recyclerVTestHistory.setVisibility(View.VISIBLE);
                    rlNoData.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    recyclerVTestHistory.setVisibility(View.GONE);
                    rlNoData.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });

        if (itemList.isEmpty()) {
            recyclerVTestHistory.setVisibility(View.GONE);
            rlNoData.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setRefreshing(false);
        } else {
            setAdapter();
        }
    }

    /**
     * Fetches the data persisted in database aggregated by network.
     */
    private void fetchPersistedData() {

        if (mContext != null) {
            setValues(DatabaseHandler.init(mContext).getByServerAggregation());
        } else {
            setValues(new ArrayList<TestResult>());
        }
    }

    /**
     * Parses the TestResult received into ListItem items so they can be presented on the UI.
     *
     * @param arrayList list with TestResult
     */
    public void setValues(ArrayList<TestResult> arrayList) {

        itemList = new MyLinkedMap<>();
        Collections.reverse(arrayList);

        for (TestResult data : arrayList) {

            if (!itemList.containsKey(data.server)) {
                itemList.put(data.server, new QuickResultHeader(data, ListItem.TYPE_QR_HEADER));
            }
            itemList.put(data.date, new QuickResultItem(data, ListItem.TYPE_QR_ITEM));
        }
    }

    /**
     * Add the list of results to the Adapter on the UI.
     */
    public void setAdapter() {

        ResultAdapter aggregationAdapter = new ResultAdapter(mContext,
                itemList, AdapterResultEnum.SERVER_AGG);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerVTestHistory.setLayoutManager(layoutManager);
        recyclerVTestHistory.setAdapter(aggregationAdapter);
        swipeRefreshLayout.setRefreshing(false);
    }


}