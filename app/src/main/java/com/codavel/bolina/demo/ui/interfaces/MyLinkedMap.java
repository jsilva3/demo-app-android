/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.interfaces;

import java.util.LinkedHashMap;
import java.util.Set;

/**
 * Linked Hash Map for ListItem values
 *
 * @param <K> String identifier
 * @param <V> ListItem type
 */
public class MyLinkedMap<K, V> extends LinkedHashMap<K, V> {

    /**
     * Getter for the ListItem value
     *
     * @param i ListItem type
     * @return ListItem type value
     */
    public V getValue(int i) {

        Entry<K, V> entry = this.getEntry(i);
        if (entry == null) return null;

        return entry.getValue();
    }

    /**
     * Getter for the ListItem
     *
     * @param i ListItem type value
     * @return ListItem
     */
    public Entry<K, V> getEntry(int i) {

        Set<Entry<K, V>> entries = entrySet();
        int j = 0;

        for (Entry<K, V> entry : entries) {
            if (j++ == i) {
                return entry;
            }
        }

        return null;
    }
}
