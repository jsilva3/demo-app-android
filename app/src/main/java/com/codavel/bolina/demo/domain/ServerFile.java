/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain;

/**
 * ServerFile contains filePath information, id, URL, size and name. A ContentServer contains a list
 * of those files, representing a filePath downloadable from a specific ContentServer.
 */
public class ServerFile {

    /**
     * URL to the filePath
     */
    private String urlPath;
    /**
     * File name
     */
    private String fileName;
    /**
     * File URL path
     */
    private String filePath;
    /**
     * Splitter
     */
    private static final String SPLITTER = "/";
    /**
     * Address port divider
     */
    private static final String ADDRESS_DIVIDER = ":";
    /**
     * Position containing file name after split
     */
    private static final int SPLIT_POSITION = 2;
    /**
     * File URL for upload script
     */
    private String uploadPath;
    /**
     * File size
     */
    private long fileSize;

    /**
     * Basic constructor
     *
     * @param urlAddress server URL address
     * @param port       server port
     * @param filePath   path to filePath
     * @param fileSize   filePath size
     */
    ServerFile(String urlAddress, int port, String filePath, long fileSize) {

        this.urlPath = urlAddress + ADDRESS_DIVIDER + port + filePath;
        this.fileSize = fileSize;
        this.fileName = filePath.split(SPLITTER)[SPLIT_POSITION];
        this.filePath = filePath;
    }

    /**
     * Basic constructor
     *
     * @param filePath   path to filePath
     * @param fileSize   file size
     * @param uploadPath upload path
     */
    public ServerFile(String filePath, long fileSize, String uploadPath) {

        this.fileSize = fileSize;
        this.uploadPath = uploadPath;
        this.fileName = filePath.split(SPLITTER)[SPLIT_POSITION];
        this.filePath = filePath;
    }

    /**
     * Getter for upload path
     *
     * @return upload path
     */
    public String getUploadPath() {

        return uploadPath;
    }

    /**
     * Getter for file path
     *
     * @return file path
     */
    public String getFilePath() {

        return filePath;
    }

    /**
     * Setter for file path
     *
     * @param file path to file
     */
    public void setFile(String file) {

        this.filePath = file;
    }

    /**
     * Getter for url path
     *
     * @return file url path
     */
    public String getUrlPath() {

        return urlPath;
    }

    /**
     * Getter for file size
     *
     * @return file size
     */
    public long getFileSize() {

        return fileSize;
    }

    /**
     * Getter for file name
     *
     * @return file name
     */
    public String getFileName() {

        return fileName;
    }

    /**
     * ServerFile information as a String
     *
     * @return String with file name
     */
    @Override
    public String toString() {

        return this.fileName;
    }
}