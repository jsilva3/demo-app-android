/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.tasks;

import android.os.AsyncTask;
import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.domain.ContentServer;
import com.codavel.bolina.demo.domain.ModeEnum;
import com.codavel.bolina.demo.domain.utils.RandomString;
import com.codavel.bolina.demo.domain.ServerFile;
import com.codavel.bolina.demo.domain.protocol.ProtocolEnum;
import com.codavel.bolina.demo.domain.protocol.ProtocolFactory;
import com.codavel.bolina.demo.domain.protocol.ProtocolInterface;
import com.codavel.bolina.demo.ui.fragments.FragmentHome;

import java.util.HashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

/**
 * Async task to perform a request. TestManagerTask will manage n PerformRequestTask, that will
 * consume ServerFile information from the queue to perform requests until TestManagerTask gives
 * signal to end.
 */
public class PerformRequestTask extends AsyncTask<Void, Void, Void> {
    /**
     * List of Server files shared with other PerformRequestTask
     */
    private ConcurrentLinkedQueue<ServerFile> files;
    /**
     * Mode to perform request
     */
    private ModeEnum mode;
    /**
     * Protocol to perform request
     */
    private ProtocolEnum protocol;
    /**
     * Message handler to report info to UI
     */
    private Handler messageHandler;
    /**
     * ServerFile info to request
     */
    private ServerFile fileToGet;
    /**
     * ContentServer info to request the file
     */
    private ContentServer contentServer;
    /**
     * Request task Identifier
     */
    private String identifier;
    /**
     * The time to run
     */
    private long timeToRun;
    /**
     * Start time
     */
    private long startTime;

    /**
     * Tag for logging
     */
    private static final String TAG = AppData._TAG + "[REQUEST-TASK]";
    /**
     * Identifier size
     */
    private static final int SIZE_ID = 5;
    /**
     * Message KEYS
     */
    private static final String IDENTIFIER = "identifier";
    private static final String TIME = "time";
    /**
     * Flag for cancel
     */
    private volatile boolean isCancel = false;

    /**
     * PerformRequetsTask constructor.
     *
     * @param server         ContentServer to request file
     * @param files          List of ServerFiles files information
     * @param mode           mode to perform request
     * @param protocol       protocol implementation to be used
     * @param messageHandler message the communicate with the UI
     */
    public PerformRequestTask(ContentServer server, ConcurrentLinkedQueue<ServerFile> files,
                              ModeEnum mode, ProtocolEnum protocol, Handler messageHandler,
                              long timeToRun) {

        this.startTime = System.currentTimeMillis();
        this.timeToRun = timeToRun;
        this.contentServer = server;
        this.files = files;
        this.mode = mode;
        this.messageHandler = messageHandler;
        this.protocol = protocol;
        this.identifier = new RandomString(SIZE_ID).nextString();
    }

    /**
     * Getter for the task identifier
     *
     * @return task identifier
     */
    public String getIdentifier() {

        return identifier;
    }

    /**
     * Send a start message to the UI and calls runTrialDecisor with file information until is
     * cancelled. If there's no files to consume from the shared list, it leaves.
     */
    @Override
    protected Void doInBackground(Void... voids) {

        sendStartMessage();

        while (!isCancel && (System.currentTimeMillis() - startTime < timeToRun)) {
            if (!files.isEmpty()) {
                fileToGet = files.poll();
                if (BuildConfig.TRACE) {
                    Log.i(TAG, "New request for: " + fileToGet.getFileName() + " Size: " +
                            fileToGet.getFileSize());
                }
                runTrialDecisor(protocol);
            } else if (fileToGet != null) {
                runTrialDecisor(protocol);
                if (BuildConfig.TRACE) {
                    Log.i(TAG, "New request for: " + fileToGet.getFileName() + " Size: " +
                            fileToGet.getFileSize());
                }
            } else {
                if (BuildConfig.TRACE) {
                    Log.e(TAG, "File poll is empty");
                }
                return null;
            }
        }
        if (BuildConfig.TRACE) {
            Log.i(TAG, " Thread " + identifier + " was cancelled at " +
                    System.currentTimeMillis());
        }
        return null;
    }

    public void cancel() {
        isCancel = true;
    }

    /**
     * Calls the ProtocolFactory for the given ProtocolEnum.
     *
     * @param protocolEnum ProtocolEnum to perform Trial
     */
    private void runTrialDecisor(ProtocolEnum protocolEnum) {

        ProtocolInterface protocol = new ProtocolFactory().getProtocol(protocolEnum);
        protocol.performTrial(mode, contentServer, fileToGet, messageHandler);
    }

    /**
     * Send a message to the UI with start info
     */
    private void sendStartMessage() {

        Message message = new Message();
        message.what = FragmentHome.THREAD_START;
        HashMap<String, Object> messageObject = new HashMap<>();
        messageObject.put(IDENTIFIER, this.identifier);
        messageObject.put(TIME, System.currentTimeMillis());
        message.obj = messageObject;
        messageHandler.dispatchMessage(message);
    }
}
