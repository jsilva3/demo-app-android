/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.protocol;

/**
 * Factory pattern implementation to create Protocol implementations.
 */
public class ProtocolFactory {

    /**
     * Creates a instance of ProtocolInterface implementation.
     *
     * @param protocol ProtocolEnum to be created
     * @return instance of ProtocolInterface
     */
    public ProtocolInterface getProtocol(ProtocolEnum protocol) {

        switch (protocol) {
            case BOLINA:
                return new BolinaProtocol();
            case HTTPS:
                return new HttpsProtocol();
            default:
                return null;
        }
    }
}
