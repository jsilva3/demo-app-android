/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.R;
import com.codavel.bolina.demo.modal.TestResult;
import com.codavel.bolina.demo.persistence.sql.DatabaseHandler;
import com.codavel.bolina.demo.ui.adapters.AdapterResultEnum;
import com.codavel.bolina.demo.ui.adapters.ResultAdapter;
import com.codavel.bolina.demo.ui.interfaces.ListItem;
import com.codavel.bolina.demo.ui.interfaces.MyLinkedMap;
import com.codavel.bolina.demo.ui.interfaces.QuickResultHeader;
import com.codavel.bolina.demo.ui.interfaces.QuickResultItem;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fetches TestResult persisted in database, decomposing it into ListItems to be presented on
 * ResultAdapter
 */
public class FragmentResults extends Fragment {

    @BindView(R.id.recyclerVTestHistory)
    RecyclerView recyclerVTestHistory;
    @BindView(R.id.swipeRefreshLayout)
    SwipeRefreshLayout swipeRefreshLayout;
    @BindView(R.id.rlNoData)
    RelativeLayout rlNoData;

    /**
     * TAG used for logging
     */
    private static final String TAG = AppData._TAG + "[RESULTS]";
    /**
     * The adapter to present results
     */
    private ResultAdapter resultHistoryAdapter;
    /**
     * TestResult persisted in database
     */
    private ArrayList<TestResult> arrayList;
    /**
     * Application contex
     */
    private Context mContext;
    /**
     * List of ListItem (Header and Items)
     */
    private MyLinkedMap<String, ListItem> myLinkedMap;

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {
                getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            } catch (NullPointerException e) {
                Log.e(TAG, "Exception on FragResults transition");
            }
        }
    }

    /**
     * Inflates the fragment results layout
     *
     * @param inflater           LayoutInflater to inflate layout
     * @param container          ViewGroup container
     * @param savedInstanceState Bundle with saved instance
     * @return View with layout inflated
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_results, container, false);
    }

    /**
     * Fetches data persisted on database, calling the set values method with result fetched
     *
     * @param view               View created
     * @param savedInstanceState Bundle with saved instance
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        mContext = getContext();
        ButterKnife.bind(this, view);
        swipeRefreshLayout.setRefreshing(true);

        arrayList = DatabaseHandler.init(mContext).getAllTestResultData();

        if (arrayList.size() > 0) {
            setValues(arrayList);
        } else {
            recyclerVTestHistory.setVisibility(View.GONE);
            rlNoData.setVisibility(View.VISIBLE);
            swipeRefreshLayout.setRefreshing(false);
        }

        //refresh button behaviour
        swipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {

                if (mContext != null) {
                    arrayList = DatabaseHandler.init(mContext).getAllTestResultData();
                }

                // set values into adapter
                if (arrayList.size() > 0) {
                    setValues(arrayList);
                    recyclerVTestHistory.setVisibility(View.VISIBLE);
                    rlNoData.setVisibility(View.GONE);
                    swipeRefreshLayout.setRefreshing(false);
                } else {
                    recyclerVTestHistory.setVisibility(View.GONE);
                    rlNoData.setVisibility(View.VISIBLE);
                    swipeRefreshLayout.setRefreshing(false);
                }
            }
        });
    }

    /**
     * Parses the TestResult received into ListItem items so they can be presented on the UI.
     *
     * @param arrayList list with TestResult
     */
    public void setValues(ArrayList<TestResult> arrayList) {

        myLinkedMap = new MyLinkedMap<>();
        Collections.reverse(arrayList);

        for (TestResult data : arrayList) {
            if (!myLinkedMap.containsKey(data.testId)) {
                myLinkedMap.put(data.testId, new QuickResultHeader(data, ListItem.TYPE_QR_HEADER));
            }
            myLinkedMap.put(data.date, new QuickResultItem(data, ListItem.TYPE_QR_ITEM));
        }
        setAdapter();
    }

    /**
     * Sets the adapter
     */
    public void setAdapter() {

        resultHistoryAdapter = new ResultAdapter(mContext, myLinkedMap,
                AdapterResultEnum.HISTORY_RESULT);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerVTestHistory.setLayoutManager(layoutManager);
        recyclerVTestHistory.setAdapter(resultHistoryAdapter);
        swipeRefreshLayout.setRefreshing(false);
    }

}