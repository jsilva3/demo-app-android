/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.protocol;
import com.codavel.bolina.demo.domain.utils.CommonObjects;
import java.util.concurrent.TimeUnit;

import okhttp3.OkHttpClient;

/**
 * Shared OkHttpCLient instance. It allows the reuse of ConnectionPools and Dispatchers.
 * <p>
 * Check: https://github.com/couchbase/couchbase-lite-java-core/issues/1489#issuecomment-279566304
 * <p>
 * More info: https://square.github.io/okhttp/3.x/okhttp/okhttp3/OkHttpClient.html
 * <p>
 * Alternative: https://gist.github.com/HubertWo/ad16547fecaca76e14a0016a74f19c33
 */
public class OkHttpSharedSingleton {

    /**
     * Shared OkHttpClient instance, treated as a singleton
     */
    private static OkHttpClient sharedClient;

    /**
     * Get the shared OkHtttpClient instance
     *
     * @return shared OkHtttpClient instance
     */
    public static OkHttpClient getSharedInstance() {

        if (sharedClient == null) {
            if (CommonObjects.okHttpCall_ttl != 0) {
                sharedClient = new OkHttpClient.Builder().callTimeout(CommonObjects.okHttpCall_ttl,
                        TimeUnit.MILLISECONDS).build();
            } else {
                sharedClient = new OkHttpClient.Builder().build();
            }
        }
        return sharedClient;
    }
}
