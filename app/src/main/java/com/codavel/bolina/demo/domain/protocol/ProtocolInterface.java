/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.protocol;

import android.os.Handler;

import com.codavel.bolina.demo.domain.ContentServer;
import com.codavel.bolina.demo.domain.ModeEnum;
import com.codavel.bolina.demo.domain.ServerFile;

/**
 * Interface for Protocol implementation. Each protocol should implement this interface and it's
 * methods
 */
public interface ProtocolInterface {
    /**
     * Performs the a Trial using a protocol implementation.
     *
     * @param mode               Mode to be performed
     * @param server             ContentServer of the trial
     * @param file               ServerFile of the Trial
     * @param toActivity         communication handler to UI activity
     */
    void performTrial(ModeEnum mode, ContentServer server, ServerFile file,
                      Handler toActivity);
}
