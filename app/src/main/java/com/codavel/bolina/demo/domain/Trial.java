/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain;

import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.domain.protocol.ProtocolEnum;

import org.json.JSONObject;

/**
 * Information collected from a Trial (e.g. rate, bytes transferred, time, ssid,...)
 */
public class Trial {
    /**
     * Deployment identifier
     */
    private String deploymentId;
    /**
     * Transfer unique identifier
     */
    private String transferId;
    /**
     * Network SSID
     */
    private String ssid;
    /**
     * Device information
     */
    private String device;
    /**
     * Device Model
     */
    private String model;
    /**
     * Android OS version
     */
    private String osVersion;
    /**
     * Network technology used
     */
    private String networkTech;
    /**
     * Average RTT measured
     */
    private double avgRtt;
    /**
     * Start time in milliseconds
     */
    private long startTime;
    /**
     * Stop time in milliseconds
     */
    private long stopTime;
    /**
     * Bytes transfered on Trial
     */
    private long bytesTransfered;
    /**
     * HTTP status code
     */
    private int httpRespCode;
    /**
     * ContentServer used on Trial
     */
    private ContentServer contentServer;
    /**
     * Trial ModeEnum (POST or GET)
     */
    private ModeEnum mode;
    /**
     * Trial ProtocolEnum
     */
    private ProtocolEnum protocol;
    /**
     * Trial NetworkConditionsProfile
     */
    private NetworkConditionsProfile netProfile;
    /**
     * JSON keys
     */
    private static final String DID = "did";
    private static final String ID = "id";
    private static final String PING = "ping";
    private static final String TAG = "tag";
    private static final String SERVER = "server";
    private static final String TECH = "tech";
    private static final String SSID = "ssid";
    private static final String START = "start";
    private static final String STOP = "stop";
    private static final String ELAPSED = "elapsed";
    private static final String OS_VERSION = "os_version";
    private static final String PROTOCOL = "protocol";
    private static final String RATE = "rate";
    private static final String TRIAL_ID = "trial_id";
    private static final String DEVICE = "device";
    private static final String MODEL = "model";
    private static final String ANDROID = "android";
    private static final String VERSION = "version";
    private static final String MODE = "trial_type";

    /**
     * Getter of ContentServer used on Trial
     *
     * @return ContentServer used on Trial
     */
    public ContentServer getContentServer() {

        return contentServer;
    }

    /**
     * Calculates the trial rate
     *
     * @return trial rate in Mb/s
     */
    public double getTrialRate() {

        //Trial can be a Dummy entry to comply with the UI (see FragmentHome.java#isSingleMode).
        // If so, the start or stop time will have the default value (-1) and so we do not need to
        // calculate the rate which will be 0;
        if (stopTime < 0 || startTime < 0) {
            return 0;
        }

        long elapsed = stopTime - startTime;
        return bytesTransfered * 8.0 * 1000.0 / (1024.0 * 1024.0 * elapsed);
    }

    /**
     * Setter for ContentServer used by Trial
     *
     * @param contentServer ContentServer used by Trial
     */
    public void setContentServer(ContentServer contentServer) {

        this.contentServer = contentServer;
    }

    /**
     * Getter for NetworkConditionsProfile
     *
     * @return Trial's NetworkConditionsProfile
     */
    public NetworkConditionsProfile getNetworkConditionsProfile() {

        return netProfile;
    }

    /**
     * Defines Trial's NetworkConditionsProfile
     *
     * @param netProfile Trial's NetworkConditionsProfile
     */
    public void setNetworkConditionsProfile(NetworkConditionsProfile netProfile) {

        this.netProfile = netProfile;
    }

    /**
     * Getter for ModeEnum
     *
     * @return Trial's ModeEnum
     */
    public ModeEnum getMode() {

        return mode;
    }

    /**
     * Defines Trial's ModeEnum
     *
     * @param mode Trial's ModeEnum
     */
    public void setMode(ModeEnum mode) {

        this.mode = mode;
    }

    /**
     * Getter for ProtocolEnum of Trial
     *
     * @return ProtocolEnum of Trial
     */
    public ProtocolEnum getProtocol() {

        return protocol;
    }

    /**
     * Setter for ProtocolEnum of Trial
     *
     * @param protocol ProtocolEnum of Trial
     */
    public void setProtocol(ProtocolEnum protocol) {

        this.protocol = protocol;
    }

    /**
     * Getter for connection technology used to perform the Trial.
     *
     * @return connection technology used
     */
    public String getTech() {

        return networkTech;
    }

    /**
     * Setter of connection technology used to perform the Trial.
     *
     * @param tech connection technology used
     */
    public void setTech(String tech) {

        this.networkTech = tech;
    }

    /**
     * Trial basic constructor
     */
    public Trial() {
        startTime = -1;
        stopTime = -1;
        bytesTransfered = -1;
    }

    /**
     * Trial constructor
     *
     * @param contentServer ContentServer used on Trial
     * @param mode          ModeEnum representing Trial mode
     * @param protocol      ProtocolEnum representing protocol used on Trial
     */
    public Trial(ContentServer contentServer, ModeEnum mode, ProtocolEnum protocol) {

        this.contentServer = contentServer;
        this.mode = mode;
        this.protocol = protocol;
    }

    /**
     * Getter for deployment identifier
     *
     * @return deployment identifier
     */
    public String getDeploymentID() {

        return deploymentId;
    }

    /**
     * Getter for Android OS version
     *
     * @return Android OS version
     */
    public String getOSVersion() {

        return osVersion;
    }

    /**
     * Setter of Android OS version
     *
     * @param osVersion Android OS version
     */
    public void setOSVersion(String osVersion) {

        this.osVersion = osVersion;
    }

    /**
     * Setter for deployment identifier
     *
     * @param deploymentId deployment identifier
     */
    public void setDeploymentID(String deploymentId) {

        this.deploymentId = deploymentId;
    }

    /**
     * Getter for average RTT
     *
     * @param avgRtt average RTT
     */
    public void setAvgRTT(double avgRtt) {

        this.avgRtt = avgRtt;
    }

    /**
     * Setter for the Trial start time in milliseconds
     *
     * @param startTime Trial start time in milliseconds
     */
    public void setStartTime(long startTime) {

        this.startTime = startTime;
    }

    /**
     * Defines the Trial stop time in milliseconds
     *
     * @param stopTime Trial stop time in milliseconds
     */
    public void setStopTime(long stopTime) {

        this.stopTime = stopTime;
    }

    /**
     * Setter of transfer unique identifier
     *
     * @param transferId transfer unique identifier
     */
    public void setTransferID(String transferId) {

        this.transferId = transferId;
    }

    /**
     * Setter of Trial's HTTP status code
     *
     * @param httpRespCode Trial's HTTP status code
     */
    public void setHttpRespCode(int httpRespCode) {

        this.httpRespCode = httpRespCode;
    }

    /**
     * Getter for the Trial average RTT
     *
     * @return Trial average RTT
     */
    public double getAvgRTT() {

        return avgRtt;
    }

    /**
     * Getter for start time of Trial in milliseconds
     *
     * @return start time of Trial in milliseconds
     */
    public long getStartTime() {

        return startTime;
    }

    /**
     * Getter for stop time of Trial in milliseconds
     *
     * @return stop time of Trial in milliseconds
     */
    public long getStopTime() {

        return stopTime;
    }

    /**
     * Getter for Trial unique identifier
     *
     * @return unique identifier
     */
    public String getTransferID() {

        return transferId;
    }

    /**
     * Getter for the HTTP status code
     *
     * @return HTTP status code
     */
    public int getHttpRespCode() {

        return httpRespCode;
    }

    /**
     * Getter for bytes transfered
     *
     * @return bytes transfered
     */
    public long getBytesTransfered() {

        return bytesTransfered;
    }

    public void setBytesTransfered(long bytesTransfered) {

        this.bytesTransfered = bytesTransfered;
    }

    /**
     * Getter for network SSID
     *
     * @return network SSID
     */
    public String getNetworkSSID() {

        return this.ssid;
    }

    /**
     * Setter for network SSID
     *
     * @param ssid network SSID
     */
    public void setNetworkSSID(String ssid) {

        this.ssid = ssid;
    }

    /**
     * Setter for device information
     *
     * @param device device information
     */
    public void setDevice(String device) {

        this.device = device;
    }

    /**
     * Setter of device model
     *
     * @param model device model
     */
    public void setModel(String model) {

        this.model = model;
    }

    /**
     * Clones generic info from a Trial.
     *
     * @param toClone Trial to be cloned.
     */
    public void clone(Trial toClone) {

        this.contentServer = toClone.contentServer;
        this.protocol = toClone.protocol;
        this.ssid = toClone.ssid;
        this.avgRtt = toClone.avgRtt;
    }

    /**
     * Creates a JSON object with Trial information
     *
     * @return TrialResult information as JSONObject
     */
    public JSONObject createStats() {

        JSONObject entry = new JSONObject();

        long elapsed = stopTime - startTime;
        float rate = (float) bytesTransfered * 8.0f * 1000.0f / (1024.0f * 1024.0f * elapsed);
        try {
            entry.put(DID, deploymentId);
            entry.put(ID, transferId);
            entry.put(MODE, mode.toString());
            entry.put(PING, avgRtt);
            entry.put(TAG, ANDROID);
            entry.put(SERVER, contentServer.getAddress());
            entry.put(TECH, networkTech);
            entry.put(SSID, ssid);
            entry.put(START, startTime);
            entry.put(STOP, stopTime);
            entry.put(ELAPSED, elapsed);
            entry.put(RATE, rate);
            entry.put(OS_VERSION, osVersion);
            entry.put(PROTOCOL, protocol);
            entry.put(TRIAL_ID, transferId);
            entry.put(DEVICE, device);
            entry.put(MODEL, model);
            entry.put(VERSION, BuildConfig.VERSION_NAME + "." +
                    String.valueOf(BuildConfig.VERSION_CODE));
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entry;
    }
}
