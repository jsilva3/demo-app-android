/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain;

import android.util.Log;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.BuildConfig;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.Locale;

/**
 * Profile with network conditions to be applied on the Bolina network emulator
 */
public class NetworkConditionsProfile {
    /**
     * Tag used for logging
     */
    private static final String TAG = AppData._TAG + "[NETWORK-PROFILE]";
    /**
     * Latency value
     */
    private int latency;
    /**
     * Packet loss ratio
     */
    private double plr;
    /**
     * JSON object keys
     */
    private static final String PACKET_LOSS_RATIO = "packet_loss_ratio";
    private static final String PACKET_LOSS = "PACKET_LOSS";
    private static final String RTT = "rtt";
    private static final String INTERFACE = "INTERFACE";
    private static final String DEFAULT = "default";
    private static final String OUTGOING = "OUTGOING";
    private static final String INCOMING = "INCOMING";
    private static final String LATENCY = "LATENCY";

    /**
     * Constructor
     *
     * @param latency latency
     * @param plr     packet loss ratio
     */
    public NetworkConditionsProfile(int latency, Double plr) {
        this.latency = latency;
        this.plr = plr;
    }

    /**
     * Creates JSONObject formatted like the Network Emulator API is expecting. The latency from the
     * profile is distributed between Incoming and Outgoing interface so the RTT is equally
     * incremented.
     *
     * @return JSONObject with Network Emulator API format
     */
    public JSONObject createJSONObject() {

        try {
            JSONObject networkProfile = new JSONObject();
            networkProfile.put(INTERFACE, DEFAULT);

            JSONObject incoming = new JSONObject();
            incoming.put(PACKET_LOSS, plr);
            incoming.put(LATENCY, latency / 2);

            networkProfile.put(INCOMING, incoming);

            JSONObject outgoing = new JSONObject();
            outgoing.put(PACKET_LOSS, plr);
            outgoing.put(LATENCY, latency / 2);

            networkProfile.put(OUTGOING, outgoing);

            return networkProfile;
        } catch (JSONException e) {
            Log.e(TAG, "Exception creating JSON object to NetEmulator");
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            }
        } catch (NumberFormatException e) {
            Log.e(TAG, "Make sure the PLR value in JSON config is a valid Double " +
                    "representation (e.g. \"0.10\" ");
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            }
        }
        return new JSONObject();
    }

    /**
     * Creates JSONObject containing information about the network conditions to be applied to the
     * emulator to be sent in stats.
     *
     * @return JSONObject containing the network conditions to be applied
     * @throws JSONException
     */
    public JSONObject createStatsJSONObject() {

        try {
            JSONObject networkProfile = new JSONObject();
            networkProfile.put(RTT, latency);
            networkProfile.put(PACKET_LOSS_RATIO, String.format(Locale.US, "%.2f", plr));
            return networkProfile;
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return new JSONObject();
    }

    /**
     * Network Profile as a String
     *
     * @return Network Profile as a String
     */
    @Override
    public String toString() {

        return "Latency: " + latency + ", PLR: " + plr;
    }
}
