/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.domain.NetworkConditionsProfile;
import com.codavel.bolina.demo.domain.protocol.OkHttpSharedSingleton;

import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import okhttp3.ConnectionSpec;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Assync task to post a new network profile to Bolina Network Emulator
 */
public class ApplyNetworkProfile extends AsyncTask<Void, Void, Void> {
    /**
     * TAG for logging on LOG
     */
    private static final String TAG = AppData._TAG + "[APPLY-NET-PROF]";
    /**
     * URL scheme
     */
    private static final String SCHEME = "http://";

    private static final String PORT = "8888";

    private static final String URL_PATH = "/config";
    /**
     * Separator used to separate server name or IP from server port
     */
    private static final String SEPARATOR = ":";
    /**
     * Okhttp client instance
     */
    private OkHttpClient client;
    /**
     * Http request
     */
    private Request request;
    /**
     * Okhttp media type for POST body definition
     */
    private static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    /**
     * Constructor
     *
     * @param address network emulator server address
     * @param profile NetworkConditionsProfile
     */
    public ApplyNetworkProfile(String address, NetworkConditionsProfile profile) {

        client = OkHttpSharedSingleton.getSharedInstance().newBuilder()
                .connectionSpecs(Arrays.asList(ConnectionSpec.MODERN_TLS, ConnectionSpec.CLEARTEXT))
                .build();
        JSONObject newProfile = profile.createJSONObject();

        RequestBody body = RequestBody.create(JSON, newProfile.toString());
        String urlString = SCHEME + address + SEPARATOR + PORT + URL_PATH;

        request = new Request.Builder()
                .url(urlString).post(body).build();
        if (BuildConfig.TRACE) {
            Log.d(TAG, "Posting New Network Profile:\n" + request.url().toString());
            Log.d(TAG, "PROFILE:\n" + newProfile.toString());
            Log.d(TAG, "URL:\n" + urlString);
        }
    }

    /**
     * Performs N_ATTEMPTS requests in background
     *
     * @param voids parameters
     * @return time spent
     */
    @Override
    protected Void doInBackground(Void... voids) {

        Response response = null;
        try {
            response = client.newCall(request).execute();

            if (!response.isSuccessful()) throw new IOException("Unexpected code " + response);

            if (BuildConfig.TRACE) {
                Log.d(TAG, "RESPONSE: " + response.body().string());
            }
        } catch (IOException e) {
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            } else {
                Log.e(TAG, e.getMessage());
            }
        }

        if (response != null) {
            response.close();
        }

        // force shutdown
        client.dispatcher().executorService().shutdown();
        client.connectionPool().evictAll();

        return null;
    }
}