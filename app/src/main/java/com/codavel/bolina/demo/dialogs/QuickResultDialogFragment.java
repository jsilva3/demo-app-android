/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.dialogs;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.RelativeLayout;

import com.codavel.bolina.demo.R;
import com.codavel.bolina.demo.modal.TestResult;
import com.codavel.bolina.demo.ui.adapters.AdapterResultEnum;
import com.codavel.bolina.demo.ui.adapters.ResultAdapter;
import com.codavel.bolina.demo.ui.interfaces.ListItem;
import com.codavel.bolina.demo.ui.interfaces.MyLinkedMap;
import com.codavel.bolina.demo.ui.interfaces.QuickResultHeader;
import com.codavel.bolina.demo.ui.interfaces.QuickResultItem;

import java.util.ArrayList;
import java.util.Collections;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Dialog to present results
 */
public class QuickResultDialogFragment extends DialogFragment {

    @BindView(R.id.rlClose)
    RelativeLayout rlClose;

    @BindView(R.id.recyclerQuick)
    RecyclerView recyclerQuick;

    private Context mContext;
    private ArrayList<TestResult> testDataArrayList;
    private MyLinkedMap<String, ListItem> listItem = new MyLinkedMap<>();

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        testDataArrayList = getArguments().getParcelableArrayList("quickResult");
    }


    @Override
    public void onActivityCreated(Bundle arg0) {
        super.onActivityCreated(arg0);
        getDialog().getWindow()
                .getAttributes().windowAnimations = R.style.DialogAnimation;
    }

    @Override
    public int getTheme() {
        return R.style.FullScreenDialog;
    }


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.dialog_fragment_quick_result, container,
                false);
        getDialog().getWindow().setSoftInputMode(WindowManager.LayoutParams.FLAG_FULLSCREEN);
        return view;
    }


    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        mContext = getActivity();
        ButterKnife.bind(this, view);

        if (!testDataArrayList.isEmpty())
            setValues();

        rlClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                getDialog().dismiss();
            }
        });

    }

    private void setAdapter() {
        ResultAdapter quickResultAdapter = new ResultAdapter(mContext, listItem,
                AdapterResultEnum.TEST_RESULT);
        final LinearLayoutManager layoutManager = new LinearLayoutManager(mContext);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerQuick.setLayoutManager(layoutManager);
        recyclerQuick.setAdapter(quickResultAdapter);

    }

    /**
     * Defines the values to be added into
     */
    public void setValues() {

        Collections.reverse(testDataArrayList);

        for (TestResult data : testDataArrayList) {
            if (!listItem.containsKey(data.server)) {
                listItem.put(data.server, new QuickResultHeader(data, ListItem.TYPE_QR_HEADER));
                listItem.put(data.date, new QuickResultItem(data, ListItem.TYPE_QR_ITEM));
            } else {
                listItem.put(data.date, new QuickResultItem(data, ListItem.TYPE_QR_ITEM));
            }
        }
        setAdapter();
    }


}
