/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.fragments;

import android.content.Context;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.os.Message;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.CardView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.R;
import com.codavel.bolina.demo.dialogs.QuickResultDialogFragment;
import com.codavel.bolina.demo.domain.ContentServer;
import com.codavel.bolina.demo.domain.ModeEnum;
import com.codavel.bolina.demo.domain.NetworkConditionsProfile;
import com.codavel.bolina.demo.domain.ServerFile;
import com.codavel.bolina.demo.domain.TestProfile;
import com.codavel.bolina.demo.domain.Trial;
import com.codavel.bolina.demo.domain.TrialResult;
import com.codavel.bolina.demo.domain.protocol.ProtocolEnum;
import com.codavel.bolina.demo.domain.tasks.ApplyNetworkProfile;
import com.codavel.bolina.demo.domain.tasks.AsyncResponse;
import com.codavel.bolina.demo.domain.tasks.PerformRequestTask;
import com.codavel.bolina.demo.domain.tasks.PingRequestTask;
import com.codavel.bolina.demo.domain.utils.CommonObjects;
import com.codavel.bolina.demo.domain.utils.Connectivity;
import com.codavel.bolina.demo.domain.utils.RandomString;
import com.codavel.bolina.demo.modal.TestResult;
import com.codavel.bolina.demo.persistence.sql.DatabaseHandler;
import com.codavel.bolina.demo.ui.animation.speedMeter.PointerSpeedometer;
import com.codavel.bolina.interceptor.okhttp3.BolinaConfiguration;
import com.codavel.bolina.interceptor.okhttp3.InterceptorSingleton;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Random;
import java.util.concurrent.BlockingQueue;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.Executor;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.RejectedExecutionException;
import java.util.concurrent.ThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import butterknife.BindView;
import butterknife.ButterKnife;

import static android.os.AsyncTask.THREAD_POOL_EXECUTOR;

/**
 * Fragment to handle the application main page
 */
public class FragmentHome extends Fragment {
    @BindView(R.id.tvProtocol)
    TextView tvProtocol;
    @BindView(R.id.tvPing)
    TextView tvPing;
    @BindView(R.id.tvDownload)
    TextView tvDownload;
    @BindView(R.id.tvUpload)
    TextView tvUpload;
    @BindView(R.id.speedometer)
    PointerSpeedometer speedometer;
    @BindView(R.id.rlGo)
    RelativeLayout rlGo;
    @BindView(R.id.rlSpeedometer)
    RelativeLayout rlSpeedometer;
    @BindView(R.id.tvGo)
    TextView tvGo;
    @BindView(R.id.cvSpeedMeter)
    CardView cvSpeedMeter;
    @BindView(R.id.llResult)
    LinearLayout llResult;
    @BindView(R.id.tvDownloadRes)
    TextView tvDownloadRes;
    @BindView(R.id.tvUploadRes)
    TextView tvUploadRes;
    @BindView(R.id.tvPingRes)
    TextView tvPingRes;
    @BindView(R.id.tvProtocolRes)
    TextView tvProtocolRes;
    @BindView(R.id.llServerName)
    LinearLayout llServerName;
    @BindView(R.id.tvServer)
    TextView tvServer;
    @BindView(R.id.tvCity)
    TextView tvCity;
    @BindView(R.id.tvNetwork)
    TextView tvNetwork;
    @BindView(R.id.ivServer)
    ImageView ivServer;
    @BindView(R.id.ivNetwork)
    ImageView ivNetwork;
    @BindView(R.id.llServer)
    LinearLayout llServer;
    @BindView(R.id.llCloseTest)
    LinearLayout llCloseTest;
    /**
     * Separator used to separate server namr or IP from server port
     */
    private static final String SEPARATOR = ":";
    /**
     * Directory containing the files for upload
     */
    private static String filesDir;
    /**
     * Connection technology used (WIFI, 3G, etc..,)
     */
    private String connectionTech;
    /**
     * Device information
     */
    private String device;
    /**
     * Device model information
     */
    private String model;
    /**
     * Device Android OS version
     */
    private String osVersion;
    /**
     * Variable to analyze the maximum rate seen on Test
     */
    private double maxRateSeen = 0;
    /**
     * TAG used for logging
     */
    private static final String TAG = AppData._TAG + "[HOME]";
    /**
     * String for message with no value
     */
    private static final String NO_VALUE = "";
    /**
     * Path used to Ping content server and get RTT
     */
    private static final String PING_PATH = "/speedtest/latency.txt";
    /**
     * Stats to be appended to deployment id
     */
    private static final String STATS = "stats-";
    /**
     * JSON key for network conditions
     */
    private static final String NETWORK_CONDITIONS = "network_conditions";
    /**
     * JSON key number of threads in profile
     */
    private static final String N_THREADS = "n_threads";
    /**
     * Flag to check if the Test is single mode (only Downloads or Uploads).
     */
    private boolean isSingleMode = false;
    /**
     * Deployment identifier
     */
    private String bolinaDeployId;
    /**
     * Codes used to handle messages received from TrialManagerAsyncTask.
     */
    public static final int TRIAL_START = 5000;
    public static final int LISTEN_UPDATES = 500;
    public static final int TRIAL_END = 5001;
    public static final int CANCELLED = 8000;
    public static final int TEST_END = 9000;
    public static final int RESET = 9001;

    public static final int THREAD_START = 4000;
    public static final int SET_MODE = 6000;
    public static final int SET_PROTOCOL = 7000;
    public static final int PROTOCOL_CHANGE = 7001;
    public static final int NEW_SSID = 7002;
    public static final int START_TEST = 7003;

    public static final int PING_FAIL = -1000;
    public static final int PING_START = 1000;
    public static final int PING_UPDATE = 1001;
    public static final int PING_FINISH = 1002;

    public static final int DOWNLOAD_UPDATE = 2000;
    public static final int DOWNLOAD_START = 2002;

    public static final int UPLOAD_START = 3000;
    public static final int UPLOAD_UPDATE = 3001;

    /**
     * Number of places to round value before present in UI
     */
    public static final int ROUND_PLACES = 2;
    /**
     * Size of trial/server identifier.
     */
    public static final int TRIAL_SERVER_ID_SIZE = 5;
    /**
     * MESSAGE objects
     */
    public static final String RESET_TEXT = "---";
    public static final String BYTES_READ = "bytes_read";
    public static final String PROTOCOL = "protocol";
    public static final String SERVER = "server";
    public static final String MODE = "mode";
    public static final String SSID = "ssid";
    public static final String RTT = "rtt";
    public static final String AVG_RATE = "avg_rate";
    public static final String RATE = "rate";
    public static final String LISTEN = "toListen";
    /**
     * Dialog manager Message
     */
    public static final String DIALOG_FRAGMENT = "DIALOG_FRAGMENT";
    /**
     * Flag to control the processing of messages in handler
     */
    private boolean isRunning = false;
    /**
     * Trial Start Time
     */
    private long startTime = 0;
    /**
     * Total number of bytes
     */
    private long totalBytes = 0;
    /**
     * Trial Stop Time
     */
    private long stopTime = 0;
    /**
     * Time passed between last update
     */
    private long elapsedTime = 0;
    /**
     * Lst UI update occurance
     */
    private long lastUpdate = 0;
    /**
     * Application context
     */
    private Context mContext;
    /**
     * Test profile to be performed
     */
    private TestProfile testProfile;
    /**
     * Manager to control the Test flow
     */
    private TestManagerTask trialManager;
    /**
     * Trial running. It's used to collect metrics to send to ELK stack and aggregate with other
     * Trial creating a TrialResult (a pair of download/upload Trials) to persist in database.
     */
    private Trial trialRunning;
    /**
     * List of TrialResults to be persisted in database once they are completed.
     */
    private LinkedHashMap<String, TrialResult> trialResults;
    /**
     * List of TestResult
     */
    private ArrayList<TestResult> results;
    /**
     * Trial rate seen
     */
    private double trialRate;
    /**
     * Average RTT seen
     */
    private double averageRtt;
    /**
     * A String combining ContentServer info and a ProtocolEnum. This will identify the entries
     * in trialResults structure.
     */
    private String trialResultId;
    /**
     * Identifier to aggregate information of TrialResults that compared
     * several protocols.
     */
    private String protocolCompareID;
    /**
     * Message handler to process messages from Trial Manager.
     */
    private Handler messageHandler;

    /**
     * FragmentHome constructor
     */
    public FragmentHome() {

        this.testProfile = new TestProfile();
    }

    /**
     * Base constructor of FragmentHome receiving the TestProfile to be performed
     *
     * @param testProfile TestProfile to be performed.
     */

    public void setTestToPerfrom(@NonNull TestProfile testProfile) {

        this.testProfile = testProfile;
    }

    /**
     * On create collects information regarding Android device and OS versions
     *
     * @param savedInstanceState
     */
    @Override
    public void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setRetainInstance(true);
        mContext = getActivity();
        this.device = Settings.Secure.getString(mContext.getContentResolver(),
                Settings.Secure.ANDROID_ID);
        this.model = CommonObjects.getDeviceName();
        this.osVersion = CommonObjects.getOsVersion();
        this.trialResults = new LinkedHashMap<String, TrialResult>();
        this.filesDir = mContext.getFilesDir().getAbsolutePath();

        createMessageHandler();
    }

    /**
     * Inflates the FragmentHome layout on the View
     *
     * @param inflater           Layout inflater
     * @param container          ViewGroup container
     * @param savedInstanceState Bundle with saved instance
     * @return View
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull final LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return getView() != null ? getView() : inflater.inflate(R.layout.fragment_home, container,
                false);
    }

    /**
     * When View is created we define the adapter for the ViewPager, setup the speedometer,
     * update server and network labels and setup the click listeners.
     *
     * @param view               View created
     * @param savedInstanceState Bundle with saved instance
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);

        ButterKnife.bind(this, view);

        final ViewPager page = getActivity().findViewById(R.id.viewpager);
        new Handler().postAtTime(new Runnable() {
            @Override
            public void run() {
                if (page.getAdapter() != null) {
                    page.setOffscreenPageLimit(4);
                    page.getAdapter().notifyDataSetChanged();
                }
            }
        }, 0);

        speedometer.speedPercentTo(0);
        speedometer.setTickNumber(8);
        speedometer.setTickPadding((int) speedometer.dpTOpx(36));
        speedometer.setSpeedometerColor(ContextCompat.getColor(mContext, R.color.grey_color));

        // get server name and network and set value
        setServerAndNetwork();

        // all click listener operations performed
        onClickListeners();
    }

    /**
     * Stops Bolina interceptor
     */
    @Override
    public void onDestroy() {

        super.onDestroy();
        stopBolinaServiceSDK();
    }

    /**
     * Behaviour definition for button's listeners('Go',servers list and cancel).
     */
    public void onClickListeners() {
        /*
         * Checks the connection, clears the data list from previous test, update UI label and
         * call the TestManager to run the Test.
         */
        tvGo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if (!isTestProfileValid()) {
                    llServer.performClick();
                } else {
                    //checks for internet connection
                    if (Connectivity.getConnectivityStatus(mContext) == Connectivity.TYPE_NOT_CONNECTED) {
                        CommonObjects.showToast(mContext, getString(R.string.error_internet));
                        return;
                    }

                    InterceptorSingleton.startInterceptor(mContext);

                    llResult.setVisibility(View.GONE);

                    //RUN TEST
                    trialManager = new TestManagerTask();
                    trialManager.executeOnExecutor(THREAD_POOL_EXECUTOR);
                }
            }
        });

        /*
         * Updates UI labels and cancels the Test
         */
        llCloseTest.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                isRunning = false;

                resetValues();
                trialManager.cancel(true);
                clearUIData();

                stopBolinaServiceSDK();
            }
        });
    }

    /**
     * Check if TestProfile is valid to run test
     *
     * @return true if TestProfile is valid to run, otherwise false
     */
    private boolean isTestProfileValid() {
        if (testProfile == null || testProfile.getContentServers().isEmpty() ||
                testProfile.getFiles().isEmpty() || testProfile.getModes().isEmpty()
                || testProfile.getProtocols().isEmpty()) {
            return false;
        }
        return true;
    }

    /**
     * Creates and initiates the Handler for message communication with running threads
     */
    private void createMessageHandler() {
        messageHandler = new Handler(Looper.getMainLooper()) {

            /**
             * Customization of Message handling for our input message.
             *
             * @param inputMessage the Message to be processed.
             */
            @Override
            public void handleMessage(final Message inputMessage) {

                final HashMap<String, Object> messageObject = (HashMap<String, Object>)
                        inputMessage.obj;

                if (inputMessage.what == FragmentHome.LISTEN_UPDATES) {
                    messageHandler.removeMessages(FragmentHome.UPLOAD_UPDATE);
                    messageHandler.removeMessages(FragmentHome.DOWNLOAD_UPDATE);

                    isRunning = (boolean) ((HashMap<String, Object>) inputMessage.obj).get(LISTEN);
                }

                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            switch (inputMessage.what) {
                                case START_TEST: {
                                    speedometer.setWithTremble(false);
                                    rlGo.setVisibility(View.GONE);
                                    rlSpeedometer.setVisibility(View.VISIBLE);
                                    llCloseTest.setVisibility(View.VISIBLE);
                                    break;
                                }
                                case PROTOCOL_CHANGE: {
                                    tvUpload.setText(RESET_TEXT);
                                    tvDownload.setText(RESET_TEXT);
                                    tvPing.setText(RESET_TEXT);
                                    break;
                                }
                                case SET_PROTOCOL: {
                                    tvProtocol.setText((String) messageObject.get(PROTOCOL));
                                    break;
                                }
                                case RESET: {
                                    messageHandler.removeMessages(FragmentHome.DOWNLOAD_UPDATE);
                                    messageHandler.removeMessages(FragmentHome.UPLOAD_UPDATE);
                                    break;
                                }
                                case TRIAL_START: {
                                    tvServer.setText((String) messageObject.get(SERVER));
                                    break;
                                }
                                case TRIAL_END: {
                                    isRunning = false;
                                    speedometer.speedPercentTo(0);
                                    speedometer.setSpeedometerColor(ContextCompat.getColor(mContext,
                                            R.color.grey_color));
                                    switch ((ModeEnum) messageObject.get(MODE)) {
                                        case POST: {
                                            tvUpload.setText((String) messageObject.get(RATE));
                                            messageHandler.removeMessages(FragmentHome.
                                                    UPLOAD_UPDATE);
                                            break;
                                        }
                                        case GET: {
                                            tvDownload.setText((String) messageObject.get(
                                                    RATE));
                                            messageHandler.removeMessages(FragmentHome.
                                                    DOWNLOAD_UPDATE);
                                            break;
                                        }
                                        default: {
                                            break;
                                        }
                                    }
                                    break;
                                }
                                case NEW_SSID: {
                                    tvNetwork.setText((String) messageObject.get(SSID));
                                    break;
                                }
                                case TEST_END: {
                                    clearUIData();
                                    break;
                                }
                                case SET_MODE: {
                                    switch ((ModeEnum) messageObject.get(MODE)) {
                                        case POST: {
                                            speedometer.setSpeedometerColor(ContextCompat.getColor
                                                    (mContext, R.color.upload_color));
                                            break;
                                        }
                                        case GET: {
                                            speedometer.setSpeedometerColor(ContextCompat.getColor(
                                                    mContext, R.color.download_color));
                                            break;
                                        }
                                        default: {
                                            break;
                                        }
                                    }
                                    break;
                                }
                                case CANCELLED: {
                                    tvUpload.setText(RESET_TEXT);
                                    tvDownload.setText(RESET_TEXT);
                                    tvPing.setText(RESET_TEXT);
                                    break;
                                }
                                case PING_FINISH: {
                                    tvPing.setText(String.valueOf((int) messageObject.
                                            get(RTT)));
                                    averageRtt = (int) messageObject.get(RTT);
                                    break;
                                }
                                case DOWNLOAD_START: {
                                    speedometer.setSpeedometerColor(ContextCompat.getColor(mContext,
                                            R.color.download_color));
                                    break;
                                }
                                case DOWNLOAD_UPDATE: {
                                    if (isRunning) {
                                        updateSpeedRate((long) messageObject.get(BYTES_READ));
                                    }
                                    break;
                                }
                                case UPLOAD_START: {
                                    speedometer.setSpeedometerColor(ContextCompat.getColor(mContext,
                                            R.color.upload_color));
                                    break;
                                }
                                case UPLOAD_UPDATE: {
                                    if (isRunning) {
                                        updateSpeedRate((long) messageObject.get(BYTES_READ));
                                    }
                                    break;
                                }
                                default: {
                                    break;
                                }
                            }
                        }
                    });
                }
            }
        };
    }

    /**
     * Clears the running test UI.
     */
    private void clearUIData() {

        try {
            createMessageHandler();
            speedometer.speedPercentTo(0);
            speedometer.setTickNumber(8);
            speedometer.setTickPadding((int) speedometer.dpTOpx(36));
            speedometer.setSpeedometerColor(ContextCompat.getColor(mContext, R.color.grey_color));
            tvUpload.setText(RESET_TEXT);
            tvDownload.setText(RESET_TEXT);
            tvPing.setText(RESET_TEXT);

            llCloseTest.setVisibility(View.GONE);
            rlSpeedometer.setVisibility(View.GONE);
            llResult.setVisibility(View.GONE);

            rlGo.setVisibility(View.VISIBLE);
            tvGo.setVisibility(View.VISIBLE);
            llServerName.setVisibility(View.VISIBLE);
        } catch (Exception e) {
        }
    }

    /**
     * Updates the speed rates according to the number of bytes received and since the last update
     *
     * @param bytes_read bytes read
     */
    private void updateSpeedRate(long bytes_read) {

        elapsedTime = System.currentTimeMillis() - startTime;
        totalBytes += bytes_read;
        double rate = CommonObjects.calculateRate(elapsedTime, totalBytes);
        updateUISpeedometer(rate);
    }

    /**
     * Stops Bolina service.
     */
    private void stopBolinaServiceSDK() {

        try {
            //stop the Bolian only if the test have run Bolina
            if (testProfile.getProtocols().contains(ProtocolEnum.BOLINA)) {
                InterceptorSingleton.getInstance().stopInterceptor();
            }
        } catch (Exception e) {
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Presents the result of a TestProfile, after it is completed
     */
    public void showBottomSheet() {

        //test has ended so we stop Bolina service
        stopBolinaServiceSDK();

        if (!trialResults.isEmpty()) {
            final ViewPager page = getActivity().findViewById(R.id.viewpager);
            try {
                if (getActivity() != null) {
                    getActivity().runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            if (page.getAdapter() != null) {
                                page.setOffscreenPageLimit(0);
                                page.getAdapter().notifyDataSetChanged();
                                page.setCurrentItem(0);
                            }
                        }
                    });
                }
            } catch (Exception e) {
                if (BuildConfig.TRACE) {
                    e.printStackTrace();
                }
            }

            ArrayList<TestResult> data = new ArrayList<TestResult>();

            Iterator it = trialResults.entrySet().iterator();
            while (it.hasNext()) {
                Map.Entry pair = (Map.Entry) it.next();
                it.remove(); // avoids a ConcurrentModificationException
                TrialResult result = (TrialResult) pair.getValue();

                data.add(new TestResult(result.getDownloadTrial().getContentServer().toString(),
                        result.getDownloadTrial().getProtocol().toString(),
                        result.getDownloadTrial().getNetworkSSID(),
                        String.valueOf(result.getUploadTrial().getStopTime()),
                        result.getDownloadTrial().getTrialRate(),
                        result.getUploadTrial().getTrialRate(),
                        result.getAverageRTT()));
            }

            QuickResultDialogFragment dialogFragment = new QuickResultDialogFragment();
            Bundle bundle = new Bundle();

            bundle.putParcelableArrayList("quickResult", data);
            dialogFragment.setArguments(bundle);
            getFragmentManager().beginTransaction().add(dialogFragment, DIALOG_FRAGMENT).
                    commitAllowingStateLoss();
        }
    }

    /**
     * Getter for file path directory
     *
     * @return file path directory
     */
    public static String getDirPath() {

        return filesDir;
    }

    /**
     * Adjusts Speedometer according to the rate seen. Once adjusted to a value higher than the max
     * rate seen, the Speedometer no more readjusts to lower values
     *
     * @param rate rate seen
     */
    private void updateUISpeedometer(double rate) {

        if (rate > maxRateSeen) {
            maxRateSeen = rate;
            if (rate > 200) {
                speedometer.setMaxSpeed((float) (rate + 150));
            } else {
                speedometer.setMaxSpeed(200f);
            }
        }

        if (isRunning) {
            speedometer.speedTo((float) rate);
        } else {
            speedometer.speedTo((float) 0);
            resetValues();
        }
    }

    /**
     * Defines the server and Network labels
     */
    public void setServerAndNetwork() {

        try {
            if (!testProfile.getContentServers().isEmpty()) {
                tvServer.setText(testProfile.getContentServers().get(0).toString());
            } else {
                tvServer.setText("Add Server...");
            }
            tvNetwork.setText(Connectivity.getCurrentSsid(mContext));
        } catch (Exception e) {
            Log.e(TAG, "Exception getting Network information;");
            e.printStackTrace();
        }
    }

    /**
     * Between trials we reset values to default values.
     */
    private void resetValues() {

        messageHandler.removeMessages(FragmentHome.UPLOAD_UPDATE);
        messageHandler.removeMessages(FragmentHome.DOWNLOAD_UPDATE);
        totalBytes = 0;
        maxRateSeen = 0;
    }

    /**
     * Async Task to iterate through the Test's Trials and perform them until all the Trials are
     * completed
     */
    private class TestManagerTask extends AsyncTask<Void, Integer, Void> implements AsyncResponse {

        final Handler handler = new Handler();
        /**
         * List of running threads
         */
        private ArrayList<PerformRequestTask> threads = new ArrayList<>();
        /**
         * Ping test to be executed
         */
        private PingRequestTask ping;
        /**
         * Post of Network profile task
         */
        private ApplyNetworkProfile postNetworkProfile;

        /**
         * Depending on the way the app is running (DEMO_APP or TEST_APP) the manager will iterate
         * through the Trials or TestResults existing on test and run the respective Trial.
         */
        @Override
        protected Void doInBackground(Void... voids) {

            if (isCancelled()) {
                stopThreads();
                return null;
            }
            //checking if test is single mode
            if (testProfile.getModes().size() == 1) {
                isSingleMode = true;
            }

            if (BuildConfig.TRACE) {
                Log.i(TAG, "Initiate Test: \n Servers: " + testProfile.getContentServers().
                        toString() + "\n Files: " + testProfile.getFiles().toString()
                        + "\n NetProfiles: " + testProfile.getNetworkProfiles().toString());
            }
            if (isCancelled()) {
                stopThreads();
                return null;
            }
            sendUIMessage(FragmentHome.LISTEN_UPDATES, true);

            for (int i = 0; i < testProfile.getTimesToRepeat(); i++) {
                if (isCancelled()) {
                    stopThreads();
                    return null;
                }
                //If no net profiles are set in the config
                if (testProfile.isApplyNetProfilesEnable()) {
                    for (NetworkConditionsProfile netProfile : testProfile.getNetworkProfiles()) {
                        if (isCancelled()) {
                            stopThreads();
                            return null;
                        }
                        runTest(netProfile, i);
                    }
                } else {
                    if (isCancelled()) {
                        stopThreads();
                        return null;
                    }
                    runTest(null, i);
                }
            }
            sendUIMessage(FragmentHome.TEST_END, NO_VALUE);
            if (isCancelled()) {
                stopThreads();
                return null;
            } else {
                showBottomSheet();
            }
            return null;
        }

        /**
         * Iterates through the test profile running the test accordingly
         *
         * @param netProfile  NetworkProfile with network properties to be applied if any
         * @param nTimesIndex index indicating the number of repeating times
         * @return
         */
        private Void runTest(NetworkConditionsProfile netProfile, int nTimesIndex) {

            sendUIMessage(FragmentHome.START_TEST, NO_VALUE);
            ConcurrentLinkedQueue<ServerFile> files = new ConcurrentLinkedQueue<>();
            files.addAll(testProfile.getFiles());

            int corePoolSize = testProfile.getNThreads();
            int maximumPoolSize = testProfile.getNThreads();

            BlockingQueue<Runnable> workQueue = new LinkedBlockingQueue<Runnable>(maximumPoolSize);

            for (final ContentServer server : testProfile.getContentServers()) {
                sendUIMessage(FragmentHome.RESET, server.toString());
                if (isCancelled()) {
                    stopThreads();
                    return null;
                }

                protocolCompareID = new RandomString(TRIAL_SERVER_ID_SIZE).nextString();
                for (final ProtocolEnum protocol : testProfile.getProtocols()) {

                    if (isCancelled()) {
                        return null;
                    }
                    sendUIMessage(FragmentHome.SET_PROTOCOL, protocol.toString());
                    if (testProfile.isApplyNetProfilesEnable()) {
                        applyNetworkConditions(server.getAddress(), netProfile);
                    }
                    pingContentServer(server.getAddress() + SEPARATOR +
                            server.getServerPort() + PING_PATH);
                    if (isCancelled()) {
                        ping.cancel(true);
                        return null;
                    }

                    for (final ModeEnum mode : testProfile.getModes()) {
                        Executor threadPoolExecutor = new ThreadPoolExecutor(corePoolSize, maximumPoolSize, 1, TimeUnit.SECONDS, workQueue);

                        sendUIMessage(FragmentHome.LISTEN_UPDATES, true);

                        //between trials wait 1 second
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                        }
                        sendUIMessage(FragmentHome.TRIAL_START, server.toString());

                        if (isCancelled()) {
                            return null;
                        }
                        //If there are no permissions to test post
                        if (mode == ModeEnum.POST && !CommonObjects.CAN_TEST_UPLOAD) {
                            break;
                        }

                        sendUIMessage(FragmentHome.SET_MODE, mode);

                        trialRunning = new Trial();
                        trialRunning.setContentServer(server);
                        trialRunning.setMode(mode);
                        trialRunning.setAvgRTT(averageRtt);
                        trialRunning.setProtocol(protocol);

                        if (testProfile.isApplyNetProfilesEnable()) {
                            trialRunning.setNetworkConditionsProfile(netProfile);
                        } else {
                            trialRunning.setNetworkConditionsProfile(new
                                    NetworkConditionsProfile(-1, -1.0));
                        }
                        addTrialInfo();

                        startTime = System.currentTimeMillis();
                        if (BuildConfig.TRACE) {
                            Log.i(TAG, "Trial Start time: " + startTime);
                        }
                        lastUpdate = startTime;
                        trialRunning.setStartTime(startTime);

                        if (testProfile.isApplyNetProfilesEnable()) {
                            //This aggreagtes the stats in the database according to the
                            // netprofile
                            trialResultId = server.toString() + protocol.toString() + nTimesIndex +
                                    netProfile.toString();
                        } else {
                            trialResultId = server.toString() + protocol.toString() + nTimesIndex;
                        }

                        final ConcurrentLinkedQueue<ServerFile> copy = new
                                ConcurrentLinkedQueue<>(files);

                        //start the trial
                        runThreads(server, copy, mode, protocol, threadPoolExecutor);

                        //TestProfile runs during time to run value
                        while (System.currentTimeMillis() - startTime <
                                testProfile.getTimeToRun()) {
                            try {
                                Thread.sleep(2000);
                            } catch (InterruptedException e) {
                                stopThreads();
                            }
                            if (isCancelled()) {
                                stopThreads();
                                ((ThreadPoolExecutor) threadPoolExecutor).shutdownNow();
                                return null;
                            }
                        }

                        //trial for the protocol is completed
                        stopThreads();

                        stopTime = System.currentTimeMillis();
                        if (BuildConfig.TRACE) {
                            Log.i(TAG, "Trial Stop time: " + stopTime);
                        }
                        trialRunning.setStopTime(stopTime);
                        trialRunning.setBytesTransfered(totalBytes);

                        sendUIMessage(FragmentHome.TRIAL_END, mode);

                        handleDataPersistence();

                        ((ThreadPoolExecutor) threadPoolExecutor).shutdownNow();

                        //between trials wait 2 second
                        try {
                            Thread.sleep(2000);
                        } catch (InterruptedException e) {
                        }
                        resetValues();
                    }
                    sendUIMessage(FragmentHome.PROTOCOL_CHANGE, NO_VALUE);
                }
                //between trials wait 1 second
                try {
                    Thread.sleep(1000);
                } catch (InterruptedException e) {
                }
            }
            return null;
        }

        /**
         * Creates and Runs the n_threads that will perform HTTP request to the given content server
         * and using the given protocol
         *
         * @param server   ContentServer to perform request
         * @param files    list of ServerFiles to be fetched
         * @param mode     ModeEnum to perform request
         * @param protocol ProtocolEnum representing the protocol to be used
         */
        private void runThreads(ContentServer server, ConcurrentLinkedQueue<ServerFile> files,
                                ModeEnum mode, ProtocolEnum protocol, Executor threadPoolExecutor) {

            //clear the list between trials
            threads.clear();

            for (int i = 0; i < testProfile.getNThreads(); i++) {
                PerformRequestTask performRequestTask = new PerformRequestTask(server, files, mode,
                        protocol, messageHandler, testProfile.getTimeToRun());
                try {
                    performRequestTask.executeOnExecutor(threadPoolExecutor);
                    threads.add(performRequestTask);
                } catch (RejectedExecutionException e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Stop all the running threads and defines the Trial as not running so the Message Handler
         * ignore the messages received.
         */
        private void stopThreads() {

            for (PerformRequestTask task : threads) {
                try {
                    task.cancel();
                } catch (Exception e) {
                }
            }
            for (PerformRequestTask task : threads) {
                try {
                    task.get();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Sends a message to the UI with info to be updated
         *
         * @param messageType message type
         * @param value       Object containing the value to be sent
         */
        private void sendUIMessage(int messageType, Object value) {

            Message message = new Message();
            message.what = messageType;
            HashMap<String, Object> message_object;

            switch (messageType) {
                case FragmentHome.TRIAL_START: {
                    message_object = new HashMap<>();
                    message_object.put(SERVER, value);
                    message.obj = message_object;
                    break;
                }
                case FragmentHome.LISTEN_UPDATES: {
                    message_object = new HashMap<>();
                    message_object.put(LISTEN, value);
                    message.obj = message_object;
                    break;
                }
                case FragmentHome.SET_PROTOCOL: {
                    message_object = new HashMap<>();
                    message_object.put(PROTOCOL, value);
                    message.obj = message_object;
                    break;
                }
                case FragmentHome.SET_MODE: {
                    message_object = new HashMap<>();
                    message_object.put(MODE, value);
                    message.obj = message_object;
                    break;
                }
                case FragmentHome.PROTOCOL_CHANGE: {
                    break;
                }
                case FragmentHome.NEW_SSID: {
                    message_object = new HashMap<>();
                    message_object.put(SSID, value);
                    message.obj = message_object;
                    break;
                }
                case FragmentHome.PING_FINISH: {
                    message_object = new HashMap<>();
                    message_object.put(RTT, value);
                    message.obj = message_object;
                    break;
                }
                case FragmentHome.TRIAL_END: {
                    message_object = new HashMap<>();
                    message_object.put(MODE, value);
                    if (trialRunning != null) {
                        message_object.put(RATE, String.valueOf(CommonObjects.round(
                                trialRunning.getTrialRate(), ROUND_PLACES)));
                    }
                    message.obj = message_object;
                    break;
                }
                default: {
                    break;
                }
            }

            try {
                if (messageHandler != null) {
                    messageHandler.dispatchMessage(message);
                }
            } catch (Exception e) {
                Log.d(TAG, "Error sending TRIAL_START message");
                e.printStackTrace();
            }
        }

        /**
         * Pings the ContentServer to retrieve average RTT time
         *
         * @param serverAddress ContentServer server address (IP or DNS)
         */
        private void pingContentServer(String serverAddress) {

            try {
                Log.i(TAG, "Pinging " + serverAddress);
                //Do Ping
                ping = new PingRequestTask(serverAddress);
                ping.delegate = this;
                Object result = ping.execute().get();

                processFinish(result);
            } catch (Exception e) {
                Message message = new Message();
                message.what = FragmentHome.PING_FAIL;
                messageHandler.dispatchMessage(message);
                e.printStackTrace();
            }
        }

        private void applyNetworkConditions(String address, NetworkConditionsProfile profile) {
            try {
                Log.i(TAG, "Posting Network Profile to " + address + " (" + profile.toString() + ")");

                postNetworkProfile = new ApplyNetworkProfile(address, profile);
                postNetworkProfile.execute().get();
            } catch (Exception e) {
                if (BuildConfig.TRACE) {
                    e.printStackTrace();
                }
            }
        }

        /**
         * Adds information to the Trial, like network technology used, android device, model and
         * OS version.
         */
        private void addTrialInfo() {

            trialRunning.setTech(Connectivity.getConnectionType(mContext));
            trialRunning.setDevice(Settings.Secure.getString(mContext.getContentResolver(),
                    Settings.Secure.ANDROID_ID));
            trialRunning.setModel(CommonObjects.getDeviceName());
            trialRunning.setOSVersion(CommonObjects.getOsVersion());
            trialRunning.setTransferID(String.valueOf(new Random().nextLong()));
            trialRunning.setDeploymentID(STATS + getResources().getString(R.string.BOLINA_DEPLOY_ID));
            String ssid = Connectivity.getCurrentSsid(mContext);
            sendUIMessage(FragmentHome.NEW_SSID, ssid);
            trialRunning.setNetworkSSID(ssid);
        }

        /**
         * After complete a Trial, gather the stats for persistence in ELK and local SQL databases.
         */
        private void handleDataPersistence() {

            JSONObject stats = new JSONObject();
            try {
                stats = trialRunning.createStats();
                stats.put(NETWORK_CONDITIONS, trialRunning.getNetworkConditionsProfile().createStatsJSONObject());
                stats.put(N_THREADS, testProfile.getNThreads());
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (BuildConfig.TRACE) {
                Log.i(TAG, "JSON STATS TO SEND: \n" + stats.toString());
            }
            InterceptorSingleton.getInstance().sendStats(stats);

            TrialResult result;
            if (trialResults.containsKey(trialResultId)) {
                result = trialResults.get(trialResultId);
                if (result.addTrial(trialRunning)) {
                    DatabaseHandler.init(mContext).addTrialResult(result);
                }
            } else {
                result = new TrialResult();
                result.setProtocolCompareId(protocolCompareID);
                result.addTrial(trialRunning);
                trialResults.put(trialResultId, result);
            }

            //The UI is prepared to present downloads and uploads. If the test is on single mode
            // then a dummy Trial with default values is added to showBottomResults() in the end
            if (isSingleMode) {
                Trial dummyTrial = new Trial();
                dummyTrial.clone(trialRunning);
                if (trialRunning.getMode() == ModeEnum.GET) {
                    dummyTrial.setMode(ModeEnum.POST);
                    dummyTrial.setStopTime(System.currentTimeMillis());
                } else {
                    dummyTrial.setMode(ModeEnum.GET);
                    dummyTrial.setStopTime(System.currentTimeMillis());
                }
                result.addTrial(dummyTrial);
            }
            resetValues();
        }

        @Override
        protected void onPostExecute(Void aVoid) {

            super.onPostExecute(aVoid);
            if (ping != null) {
                ping.cancel(true);
            }
        }

        /**
         * Notifies the UI with ping result
         *
         * @param pingResult the result of the ping
         */
        @Override
        public void processFinish(Object pingResult) {

            sendUIMessage(FragmentHome.PING_FINISH, pingResult);
        }
    }
}
