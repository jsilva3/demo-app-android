/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain;

import com.codavel.bolina.demo.domain.protocol.ProtocolEnum;

import java.util.ArrayList;

/**
 * TestProfile is the aggregation of a the complete Test profile. This will be used by TestManager
 * in FragmentHome to manage all the test flow.
 */
public class TestProfile {
    /**
     * Number of threads each Trial should create
     */
    private int nThreads;
    /**
     * Number of times to repeat the TestProfile
     */
    private int nTimesToRepeat;
    /**
     * Time expressed in milliseconds that each trial should perform
     */
    private long timeToRun;
    /**
     * ModeEnum defining Mode (Download/Upload/Both)
     */
    private ArrayList<ModeEnum> modes;
    /**
     * List of available ContentServers
     */
    private ArrayList<ContentServer> contentServers;
    /**
     * List of available ServerFile
     */
    private ArrayList<ServerFile> files;
    /**
     * List of ProtocolEnum to run
     */
    private ArrayList<ProtocolEnum> protocols;
    /**
     * List of {@link NetworkConditionsProfile} to run
     */
    private ArrayList<NetworkConditionsProfile> networkProfiles;
    /**
     * Factor to convert seconds into milliseconds
     */
    private static final int TO_MILLISECONDS = 1000;
    /**
     * Flag to enable/disable the appliance of network profiles
     */
    private boolean applyNetProfiles = false;
    /**
     * Flag to chek if test is valid
     */
    private boolean isTestValid;

    /**
     * TestProfile constructor defines the number of threads each Trial should contain, the number
     * of seconds each Trial should run and the Mode (Download, Upload or Both). Also initiates the
     * lists of servers, files, protocols and trials.
     *
     * @param nThreads  number of threads each Trial should contain
     * @param timeToRun number of seconds each Trial should run
     * @param modes     list of Modes (Download, Upload or Both)
     */
    public TestProfile(int nTimes, int nThreads, int timeToRun, ArrayList<ModeEnum> modes) {

        this.nThreads = nThreads;
        this.nTimesToRepeat = nTimes;
        this.timeToRun = timeToRun * TO_MILLISECONDS;
        this.modes = modes;
        this.contentServers = new ArrayList<ContentServer>();
        this.files = new ArrayList<ServerFile>();
        this.protocols = new ArrayList<ProtocolEnum>();
        this.networkProfiles = new ArrayList<NetworkConditionsProfile>();
        isTestValid = true;
    }

    /**
     * Getter for test valid
     *
     * @return true if test is valid otherwise false
     */
    public boolean getTestValid() {

        return isTestValid;
    }

    /**
     * Basic constructor of TestProfile.
     */
    public TestProfile() {

        this.nThreads = 0;
        this.timeToRun = 0;
        this.modes = new ArrayList<ModeEnum>();
        this.contentServers = new ArrayList<ContentServer>();
        this.files = new ArrayList<ServerFile>();
        this.protocols = new ArrayList<ProtocolEnum>();
        isTestValid = false;
    }

    /**
     * Getter for list of {@link NetworkConditionsProfile}
     *
     * @return list of {@link NetworkConditionsProfile}
     */
    public ArrayList<NetworkConditionsProfile> getNetworkProfiles() {

        return this.networkProfiles;
    }

    /**
     * Setter for list of {@link NetworkConditionsProfile}
     *
     * @param networkProfiles list of {@link NetworkConditionsProfile}
     */
    public void setNetworkProfiles(ArrayList<NetworkConditionsProfile> networkProfiles) {

        this.networkProfiles = networkProfiles;
    }

    /**
     * Check if there are network profiles to be applied
     *
     * @return true if there are network profiles to be applied, otherwise false
     */
    public boolean isApplyNetProfilesEnable() {

        return applyNetProfiles;
    }

    /**
     * Setter for the flag indicating that network profiles is to be applied
     *
     * @param applyNetProfiles boolean true/false to apply network profiles
     */
    public void setApplyNetProfiles(boolean applyNetProfiles) {

        this.applyNetProfiles = applyNetProfiles;
    }

    /**
     * Getter for list of Content Servers
     *
     * @return list of ProtocolEnums
     */
    public ArrayList<ProtocolEnum> getProtocols() {

        return protocols;
    }

    /**
     * Setter for server's list of protocols
     *
     * @param protocols list of ProtocolEnum
     */
    public void setProtocols(ArrayList<ProtocolEnum> protocols) {

        this.protocols = protocols;
    }

    /**
     * Getter for list of Content Servers
     *
     * @return list of Content Servers
     */
    public ArrayList<ContentServer> getContentServers() {

        return contentServers;
    }

    /**
     * Setter for server's list of available Content Servers
     *
     * @param content_servers list of ContentServers
     */
    public void setContentServers(ArrayList<ContentServer> content_servers) {

        this.contentServers = content_servers;
    }

    /**
     * Getter for list of ServerFiles
     *
     * @return list of ServerFiles
     */
    public ArrayList<ServerFile> getFiles() {

        return files;
    }

    /**
     * Setter for server's list of available files
     *
     * @param files list of ServerFiles
     */
    public void setFiles(ArrayList<ServerFile> files) {

        this.files = files;
    }

    /**
     * Getter for list Modes
     *
     * @return list of Modes
     */
    public ArrayList<ModeEnum> getModes() {

        return modes;
    }

    /**
     * Setter for list of Modes
     *
     * @return list of ModeEnum
     */
    public void setModes(ArrayList<ModeEnum> modes) {

        this.modes = modes;
    }

    /**
     * Getter for number of threads
     *
     * @return number of threads
     */
    public int getNThreads() {

        return nThreads;
    }

    /**
     * Setter for number of threads
     *
     * @param nThreads number of threads
     */
    public void setNThreads(int nThreads) {

        this.nThreads = nThreads;
    }

    /**
     * Getter for number of times the profile will repeat
     *
     * @return number of times the profile will repeat
     */
    public int getTimesToRepeat() {

        return nTimesToRepeat;
    }

    /**
     * Setter for number of times the profile will repeat
     *
     * @param nTimesToRepeat number of times the profile will repeat
     */
    public void setnTimesToRepeat(int nTimesToRepeat) {

        this.nTimesToRepeat = nTimesToRepeat;
    }


    /**
     * Getter for time to run
     *
     * @return time to run
     */
    public long getTimeToRun() {

        return timeToRun;
    }

    /**
     * Setter for time to run
     *
     * @param timeToRun time to run in seconds
     */
    public void setTimeToRun(int timeToRun) {

        this.timeToRun = timeToRun * TO_MILLISECONDS;
    }
}
