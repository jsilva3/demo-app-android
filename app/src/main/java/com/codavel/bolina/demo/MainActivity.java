/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.design.widget.BottomNavigationView;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.MenuItem;
import android.view.WindowManager;

import com.codavel.bolina.demo.domain.TestProfile;
import com.codavel.bolina.demo.domain.tasks.AsyncResponse;
import com.codavel.bolina.demo.domain.tasks.FetchRemoteConfigTask;
import com.codavel.bolina.demo.domain.utils.CommonObjects;
import com.codavel.bolina.demo.ui.adapters.ViewPagerAdapter;
import com.codavel.bolina.demo.ui.fragments.FragmentAbout;
import com.codavel.bolina.demo.ui.fragments.FragmentAggregateResult;
import com.codavel.bolina.demo.ui.fragments.FragmentHome;
import com.codavel.bolina.demo.ui.fragments.FragmentResults;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Application Main activity
 */
public class MainActivity extends AppCompatActivity implements AsyncResponse {
    /**
     * TAG used for logging
     */
    private static final String TAG = AppData._TAG + "[MAIN-ACTIVITY]";
    /**
     * Fragment to present the main page on the UI
     */
    private FragmentHome fragmentHome;
    /**
     * Menu item to be selected
     */
    private MenuItem prevMenuItem;
    /**
     * Adapter containing all the application Fragments (Speed, Results, Aggregation, Settings and
     * About)
     */
    private ViewPagerAdapter adapter;

    @BindView(R.id.bottomNavigation)
    BottomNavigationView bottomNavigation;
    @BindView(R.id.viewpager)
    ViewPager viewPager;

    /**
     * TestProfile to be performed
     */
    private TestProfile testToPerform;
    /**
     * Async task to fetch the config from Remote Config server.
     */
    private FetchRemoteConfigTask remoteConfigTask;
    /**
     * Path to directory where files are created
     */
    private static String filesDirPath;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        filesDirPath = getApplicationContext().getFilesDir().getAbsolutePath();

        //this to set delegate/listener back to this class
        remoteConfigTask = new FetchRemoteConfigTask(getApplicationContext());
        remoteConfigTask.delegate = this;
        remoteConfigTask.execute();

        getWindow().addFlags(WindowManager.LayoutParams.FLAG_KEEP_SCREEN_ON);
    }

    /**
     * Getter for file path directory
     *
     * @return file path directory
     */
    public static String getFilesPath() {

        return filesDirPath;
    }

    /**
     * Setups the Adapter initiating all the Fragment pages
     *
     * @param viewPager adapter containing all the application Fragments
     */
    private void setupViewPager(ViewPager viewPager) {

        adapter = new ViewPagerAdapter(getSupportFragmentManager());
        int limit = (adapter.getCount() > 1 ? adapter.getCount() - 1 : 4);
        fragmentHome = new FragmentHome();
        fragmentHome.setTestToPerfrom(testToPerform);
        FragmentResults fragmentResults = new FragmentResults();
        FragmentAggregateResult fragmentAggregateResult = new FragmentAggregateResult();
        FragmentAbout fragmentAbout = new FragmentAbout();

        adapter.addFragment(fragmentHome);
        adapter.addFragment(fragmentResults);
        adapter.addFragment(fragmentAggregateResult);
        adapter.addFragment(fragmentAbout);
        viewPager.setAdapter(adapter);
        viewPager.setOffscreenPageLimit(limit);
    }

    /**
     * Callback performed after the async task to fetch the Remote Configuration is completed.
     * This will create the bottom Navigation Items creating the fragment home after the TestProfile
     * is parsed from the Remote Config JSON. Also creates files to be used as upload body
     *
     * @param testToPerform TestProfile object to be performed.
     */
    @Override
    public void processFinish(Object testToPerform) {

        this.testToPerform = (TestProfile) testToPerform;

        if (this.testToPerform.getTimeToRun() != 0) {
            CommonObjects.okHttpCall_ttl = this.testToPerform.getTimeToRun();
        }
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);
        bottomNavigation.setOnNavigationItemSelectedListener(
                new BottomNavigationView.OnNavigationItemSelectedListener() {
                    @Override
                    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                        switch (item.getItemId()) {
                            case R.id.actionSpeed:
                                viewPager.setCurrentItem(0);
                                break;
                            case R.id.actionResult:
                                viewPager.setCurrentItem(1);
                                break;
                            case R.id.actionAggResult:
                                viewPager.setCurrentItem(2);
                                break;
                            case R.id.actionAbout:
                                viewPager.setCurrentItem(3);
                                break;
                        }
                        return false;
                    }
                });

        viewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageScrolled(int position, float positionOffset, int
                    positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {
                if (prevMenuItem != null) {
                    prevMenuItem.setChecked(false);
                } else {
                    bottomNavigation.getMenu().getItem(0).setChecked(false);
                }
                bottomNavigation.getMenu().getItem(position).setChecked(true);
                prevMenuItem = bottomNavigation.getMenu().getItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });

        try {
            setupViewPager(viewPager);
        } catch (Exception e) {
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            } else {
                Log.e(TAG, e.getMessage());
            }
        }
    }
}