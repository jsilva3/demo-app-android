/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codavel.bolina.demo.R;
import com.codavel.bolina.demo.modal.TestResult;
import com.codavel.bolina.demo.domain.utils.CommonObjects;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter to mmnage the fragments for aggregated results
 */
public class AggregationAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    /**
     * Application context
     */
    private Context mContext;
    /**
     * List of TestResult with results to be presented
     */
    private ArrayList<TestResult> arrayList;
    /**
     * Adapter name
     */
    private String type;

    /**
     * Public constructor
     *
     * @param mContext  application context
     * @param arrayList list of TestResult with results to be presented
     * @param type      adapter name
     */
    public AggregationAdapter(Context mContext, ArrayList<TestResult> arrayList, String type) {
        this.mContext = mContext;
        this.arrayList = arrayList;
        this.type = type;
    }

    /**
     * Inflates the fragment
     *
     * @param viewGroup ViewGroup
     * @param viewType  int indicating the view type
     * @return ViewHolder created
     */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {
        return new ItemViewHolder(LayoutInflater.from(mContext).inflate(R.layout.item_aggregate_network, viewGroup, false));
    }

    /**
     * According to the aggregation Adapter name (Network, Server or Both), shows the results
     *
     * @param viewHolder ViewHolder
     * @param position   selected position
     */
    @SuppressLint("SetTextI18n")
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        ItemViewHolder holder = (ItemViewHolder) viewHolder;
        TestResult testData = arrayList.get(position);

        if (type.equalsIgnoreCase(mContext.getResources().getString(R.string.network))) {
            holder.tvAggServer.setVisibility(View.VISIBLE);
            holder.tvAggServer.setText(testData.protocol);
            holder.tvAggNetwork.setText(testData.network);

        } else if (type.equalsIgnoreCase(mContext.getResources().getString(R.string.server))) {
            holder.tvAggServer.setVisibility(View.GONE);
            holder.tvAggNetwork.setText(testData.server.toUpperCase().charAt(0) + testData.server.substring(1, testData.server.length()) + " (" + testData.protocol + ")");
        } else {
            holder.tvAggServer.setVisibility(View.VISIBLE);
            holder.tvAggNetwork.setText(testData.network);
            holder.tvAggServer.setText(testData.server.toUpperCase().charAt(0) + testData.server.substring(1, testData.server.length()) + " (" + testData.protocol + ")");
        }
        holder.tvAggPing.setText(CommonObjects.getFormattedNumber(testData.ping) + " " + mContext.getResources().getString(R.string.ms));
        holder.tvAggDownload.setText(CommonObjects.getFormattedNumber(testData.downloadSpeed) + " " + mContext.getResources().getString(R.string.mbps));
        holder.tvAggUpload.setText(CommonObjects.getFormattedNumber(testData.uploadSpeed) + " " + mContext.getResources().getString(R.string.mbps));
    }

    /**
     * Getter for the number of adapters
     *
     * @return number of adapters
     */
    @Override
    public int getItemCount() {
        if (arrayList != null)
            return arrayList.size();
        else return 0;
    }

    /**
     * ViewHolder implementation
     */
    class ItemViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvAggPing)
        TextView tvAggPing;
        @BindView(R.id.tvAggDownload)
        TextView tvAggDownload;
        @BindView(R.id.tvAggUpload)
        TextView tvAggUpload;
        @BindView(R.id.tvAggNetwork)
        TextView tvAggNetwork;
        @BindView(R.id.tvAggServer)
        TextView tvAggServer;

        ItemViewHolder(View inflate) {
            super(inflate);
            ButterKnife.bind(this, inflate);

        }
    }
}
