/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain;

/**
 * Regarding the ContentServer, city, country, the content server address and port, Bolina server dns/ip,
 * list of ServerFiles at disposition to download.
 */
public class ContentServer {

    /**
     * City location
     */
    private String city;
    /**
     * Country location
     */
    private String country;
    /**
     * Server address (IP or DNS)
     */
    private String serverAddress;
    /**
     * Bolina server address (IP or DNS)
     */
    private String bolinaServer;
    /**
     * Server port
     */
    private int serverPort;

    /**
     * ContentServer constructor
     *
     * @param city          city location
     * @param country       country location
     * @param serverAddress server address (IP or DNS)
     * @param bolinaServer  bolina server address(IP or DNS)
     * @param port          server port
     */
    public ContentServer(String city, String country, String serverAddress, String bolinaServer,
                         int port) {

        this.city = city;
        this.country = country;
        this.serverAddress = serverAddress;
        this.serverPort = port;
        this.bolinaServer = bolinaServer;
    }

    /**
     * Getter for server address
     *
     * @return server address
     */
    public String getAddress() {

        return serverAddress;
    }

    /**
     * Getter for Bolina server address
     *
     * @return Bolina server address (IP or DNS)
     */
    public String getBolinaServer() {

        return bolinaServer;
    }

    /**
     * Getter for server city location
     *
     * @return server city location
     */
    public String getCity() {

        return city;
    }

    /**
     * Server country location getter
     *
     * @return country location
     */
    public String getCountry() {

        return this.country;
    }

    /**
     * Server port getter
     *
     * @return server port
     */
    public int getServerPort() {

        return this.serverPort;
    }

    /**
     * Contentserver information as a String
     *
     * @return String with server country and city location
     */
    @Override
    public String toString() {

        return this.country + "-" + this.city;
    }
}
