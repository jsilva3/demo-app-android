/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.utils;

import android.content.Context;
import android.os.Build;
import android.text.format.DateFormat;
import android.widget.Toast;

import com.codavel.bolina.demo.domain.ServerFile;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.NumberFormat;
import java.util.Calendar;
import java.util.Locale;

public class CommonObjects {
    /**
     * On performing uploads, if variable is true, then it will use random bytes. Otherwise the app
     * will create files on start {@link com.codavel.bolina.demo.domain.tasks.CreateFilesTask} and
     * read those files
     * {@link com.codavel.bolina.demo.domain.protocol.HttpsProtocol#runUpload(String, ServerFile)}
     */
    public static final boolean USE_RANDOM_BYTES = true;
    /**
     * TAG used for logging
     */
    public static final String TAG = "Commons";
    /**
     * Boolean to control flow
     */
    public static boolean CAN_TEST_UPLOAD = false;
    /**
     * Long value determining the {@link okhttp3.Call} time out to be applied when creating the
     * {@link com.codavel.bolina.demo.domain.protocol.OkHttpSharedSingleton}. This value is equal to
     * {@link com.codavel.bolina.demo.domain.TestProfile#timeToRun}
     */
    public static long okHttpCall_ttl = 0;

    /**
     * Getter for a double number with only two decimal places
     *
     * @param value value to be truncated
     * @return value truncated
     */
    public static String getFormattedNumber(double value) {

        Locale l = Locale.US;
        NumberFormat formatter = NumberFormat.getNumberInstance(l);
        formatter.setMinimumFractionDigits(2);
        formatter.setMaximumFractionDigits(2);
        return formatter.format(value);
    }

    /**
     * Getter for hardware device information
     *
     * @return hardware device information
     */
    public static String getDeviceName() {

        String manufacturer = Build.MANUFACTURER;
        String model = Build.MODEL;

        if (model.startsWith(manufacturer)) {
            return model;
        } else {
            return manufacturer + " " + model;
        }
    }

    /**
     * Getter for Android OS version
     *
     * @return Android OS version
     */
    public static String getOsVersion() {

        double release = Double.parseDouble(Build.VERSION.RELEASE.replaceAll(
                "(\\d+[.]\\d+)(.*)", "$1"));
        String codeName = "Unsupported";//below Jelly bean OR above Oreo
        if (release >= 4.1 && release < 4.4) codeName = "Jelly Bean";
        else if (release < 5) codeName = "Kit Kat";
        else if (release < 6) codeName = "Lollipop";
        else if (release < 7) codeName = "Marshmallow";
        else if (release < 8) codeName = "Nougat";
        else if (release < 9) codeName = "Oreo";
        return codeName + " v" + release + ", API Level: " + Build.VERSION.SDK_INT;
    }

    /**
     * Shows a Toast to the user displaying the message passed
     *
     * @param mContext application context
     * @param message  message to be displayed on Toast
     */
    public static void showToast(final Context mContext, final String message) {

        Toast.makeText(mContext, message, Toast.LENGTH_LONG).show();
    }

    /**
     * Getter for a date formatted from date presented by milliseconds.
     *
     * @param smsTimeInMillis date in milliseconds
     * @return date formatted
     */
    public static String getFormattedDate(long smsTimeInMillis) {

        Calendar smsTime = Calendar.getInstance();
        smsTime.setTimeInMillis(smsTimeInMillis);
        Calendar now = Calendar.getInstance();

        if (now.get(Calendar.DATE) == smsTime.get(Calendar.DATE)) {
            return "Today";
        } else if (now.get(Calendar.DATE) - smsTime.get(Calendar.DATE) == 1) {
            return "Yesterday";
        } else {
            return DateFormat.format("dd MMM, yyyy", smsTime).toString();
        }
    }

    /**
     * Calculate rate values in Mb/s
     *
     * @param time_elapsed in milliseconds
     * @param bytes
     * @return double in Mb/s
     */
    public static double calculateRate(long time_elapsed, long bytes) {

        double time_seconds = time_elapsed / 1000.0;
        double mega_bits = (bytes * 8) / (1024.0 * 1024.0);
        return mega_bits / time_seconds;
    }

    /**
     * Rounds a double value.
     *
     * @param value  value to be rounded
     * @param places number of places
     * @return the value rounded
     */
    public static double round(double value, int places) {

        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }
}
