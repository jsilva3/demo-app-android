/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import java.util.ArrayList;
import java.util.List;

/**
 * Page adapter for Fragments
 */
public class ViewPagerAdapter extends FragmentPagerAdapter {
    /**
     * List of fragments
     */
    private final List<Fragment> mFragmentList = new ArrayList<>();

    /**
     * Public constructor
     *
     * @param manager Fragment manager
     */
    public ViewPagerAdapter(FragmentManager manager) {

        super(manager);
    }

    /**
     * Getter for the fragment in the specified position
     *
     * @param position position of Fragment intended
     * @return Fragment intended
     */
    @Override
    public Fragment getItem(int position) {

        return mFragmentList.get(position);
    }

    /**
     * Counter for the number of Fragments in the PAge adapter
     *
     * @return
     */
    @Override
    public int getCount() {

        return mFragmentList.size();
    }

    /**
     * Adds a fragment to the list
     *
     * @param fragment
     */
    public void addFragment(Fragment fragment) {

        mFragmentList.add(fragment);
    }

}