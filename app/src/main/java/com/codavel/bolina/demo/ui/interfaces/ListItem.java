/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.interfaces;

/**
 * Interface to define the type of ListItem
 */
public interface ListItem {
    /**
     * Result Header
     */
    int TYPE_QR_HEADER = 0;
    /**
     * Result Item
     */
    int TYPE_QR_ITEM = 1;
    int TYPE_ITEM = 2;

    /**
     * Getter for type of ListItem
     *
     * @return type of ListItem
     */
    int getType();
}
