/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.utils;

import android.os.AsyncTask;
import android.util.Log;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.domain.ContentServer;
import com.codavel.bolina.demo.domain.ModeEnum;
import com.codavel.bolina.demo.domain.NetworkConditionsProfile;
import com.codavel.bolina.demo.domain.ServerFile;
import com.codavel.bolina.demo.domain.TestProfile;
import com.codavel.bolina.demo.domain.protocol.ProtocolEnum;
import com.codavel.bolina.demo.domain.tasks.AsyncResponse;
import com.codavel.bolina.demo.domain.tasks.CreateFilesTask;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Parses a JSON configuration file into a TestProfile object.
 */
public class JSONParser implements AsyncResponse {
    /**
     * Tag to be used on Logs.
     */
    private static final String TAG = AppData._TAG + "[JSON-PARSER]";
    /**
     * JSON Tags for parsing
     */
    private static final String MODE = "mode";
    private static final String GET = "GET";
    private static final String POST = "POST";
    private static final String BOTH = "BOTH";
    private static final String N_THREADS = "n_threads";
    private static final String N_TIMES = "n_times";
    private static final String REPEAT_TIMES = "repeat_times";
    private static final String TIME_SECONDS = "time_seconds";
    private static final String CITY = "city";
    private static final String COUNTRY = "country";
    private static final String PLR = "plr";
    private static final String LATENCY = "latency";
    private static final String ADDRESS = "address";
    private static final String BOLINA_SERVER = "bolina_server";
    private static final String PORT = "port";
    private static final String UPLOAD_PATH = "upload_path";
    private static final String DOWNL_FILES = "download_files";
    private static final String FILE = "file";
    private static final String FILES = "files";
    private static final String SIZE = "size";
    private static final String HTTPS = "HTTPS";
    private static final String BOLINA = "BOLINA";
    private static final String HTTP = "HTTP";
    private static final String PROTOCOLS = "protocols";
    private static final String SERVERS = "servers";
    private static final String NETWORK_PROFILES = "network_conditions";

    /**
     * TestProfile parsed from configuration file
     */
    private TestProfile testProfile;

    /**
     * Constructor of JSONParser with a JSONObject. Starts the process of parsing. This consist in
     * creating a TestProfile, filling it with the list of protocols and the list of servers based
     * on the information parsed.
     *
     * @param jsonObject JSONObject to be parsed
     */
    public JSONParser(JSONObject jsonObject) {

        try {
            //creating test
            initiateTest(jsonObject);

            //defining file list
            defineFileList(jsonObject.getJSONObject(FILES));

            //define protocol list
            defineProtocolList(jsonObject.getJSONArray(PROTOCOLS));

            //defining content servers
            defineServerList(jsonObject.getJSONArray(SERVERS));

            if (!jsonObject.isNull(NETWORK_PROFILES)) {
                testProfile.setApplyNetProfiles(true);
                //defining network conditions profiles
                defineNetworkConditionsProfiles(jsonObject.getJSONArray(NETWORK_PROFILES));
            }
            validateTest();
        } catch (JSONException e) {
            Log.e(TAG, "JSON invalid");
            testProfile = new TestProfile();
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Validates if JSON parsed has a valid server address.
     */
    private void validateTest() {
        for (ContentServer server : testProfile.getContentServers()) {
            if (server.getAddress().equalsIgnoreCase("<Server-address-here>")) {
                Log.e(TAG, "Bad server address in config file");
                testProfile = new TestProfile();
                break;
            }
        }
    }

    /**
     * Call of TestProfile's constructor, based on "mode"(Download,Upload or both), "n_threads" (the
     * number of threads for each Trial), "time_seconds" (number of seconds a Trial should run) and
     * teh TestProfile Mode(Download, Upload or Both).
     *
     * @param obj JSONObject received from remote config server.
     */
    private void initiateTest(JSONObject obj) {

        String modeString = null;
        try {
            modeString = obj.getString(MODE);
            ArrayList<ModeEnum> modes = new ArrayList<ModeEnum>();
            switch (modeString) {
                case GET: {
                    modes.add(ModeEnum.GET);
                    break;
                }
                case POST: {
                    modes.add(ModeEnum.POST);
                    break;
                }
                case BOTH: {
                    modes.add(ModeEnum.GET);
                    modes.add(ModeEnum.POST);
                    break;
                }
                default: {
                    Log.d(TAG, "Unknown mode: " + modeString);
                    return;
                }
            }
            testProfile = new TestProfile((int) obj.get(REPEAT_TIMES), (int) obj.get(N_THREADS),
                    (int) obj.get(TIME_SECONDS), modes);
        } catch (JSONException e) {
            Log.e(TAG, "Error creating test");
            testProfile = new TestProfile();
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Getter for the TestProfile parsed.
     *
     * @return Test parsed;
     */
    public TestProfile getTestParsed() {

        return testProfile;
    }

    /**
     * Parses the list of servers (NetworkProtocols) from the config.
     *
     * @param arrayNetworkProtocols JSONArray object with the list of Network Protocols
     */
    private void defineNetworkConditionsProfiles(JSONArray arrayNetworkProtocols) {

        ArrayList<NetworkConditionsProfile> networkProfiles = testProfile.getNetworkProfiles();
        try {
            for (int i = 0; i < arrayNetworkProtocols.length(); i++) {
                JSONObject profileObj = arrayNetworkProtocols.getJSONObject(i);

                NetworkConditionsProfile profile = new NetworkConditionsProfile((int)
                        profileObj.get(LATENCY), (Double) profileObj.get(PLR));
                networkProfiles.add(profile);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Invalid Network Condition Profile in JSON");
            testProfile = new TestProfile();
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Parses the list of servers (ContentServer) from the config.
     *
     * @param arrayServers JSONArray object with the list of servers;
     */
    private void defineServerList(JSONArray arrayServers) {

        ArrayList<ContentServer> contentServers = testProfile.getContentServers();
        try {
            for (int i = 0; i < arrayServers.length(); i++) {
                JSONObject server_obj = arrayServers.getJSONObject(i);
                ContentServer server = new ContentServer(server_obj.getString(CITY),
                        server_obj.getString(COUNTRY),
                        server_obj.getString(ADDRESS),
                        BOLINA_SERVER,
                        server_obj.getInt(PORT));
                contentServers.add(server);
            }
        } catch (JSONException e) {
            Log.e(TAG, "Invalid JSON");
            testProfile = new TestProfile();
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Defines the list of files (ServerFile)
     *
     * @param filesObj JSON object with ServerFiles info
     */
    private void defineFileList(JSONObject filesObj) {

        ArrayList<ServerFile> fileList = testProfile.getFiles();

        try {
            String uploadPath = filesObj.getString(UPLOAD_PATH);
            JSONArray files = filesObj.getJSONArray(DOWNL_FILES);

            for (int i = 0; i < files.length(); i++) {

                //We need to create the file to be uploaded, but we only need one copy of each file
                boolean createFile = true;
                JSONObject fileJson = files.getJSONObject(i);
                int repeat_times = fileJson.getInt(N_TIMES);

                for (int j = 0; j < repeat_times; j++) {
                    ServerFile file = new ServerFile(fileJson.getString(FILE),
                            fileJson.getLong(SIZE),
                            uploadPath);

                    //if use random bytes is defined as false in the build configuration, then it
                    // need to create the files to be read on uploads
                    if (!CommonObjects.USE_RANDOM_BYTES) {
                        if (createFile && CommonObjects.CAN_TEST_UPLOAD) {
                            createFile = false;
                            CreateFilesTask createFilesTask = new CreateFilesTask(file.getFileName(),
                                    file.getFileSize());
                            createFilesTask.delegate = this;
                            createFilesTask.executeOnExecutor(AsyncTask.THREAD_POOL_EXECUTOR);
                        }
                    }
                    fileList.add(file);
                }
            }
        } catch (JSONException e) {
            Log.e(TAG, "Invalid JSON");
            testProfile = new TestProfile();
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Defines the list of protocols parsed from the remote config. Each protocol is
     *
     * @param arrayProtocols JSONArray with the list of protocols
     */
    private void defineProtocolList(JSONArray arrayProtocols) {

        //Defining protocols
        ArrayList<ProtocolEnum> protocolEnums = testProfile.getProtocols();

        for (int i = 0; i < arrayProtocols.length(); i++) {
            String serverObj = null;
            try {
                serverObj = (String) arrayProtocols.get(i);
            } catch (JSONException e) {
                Log.e(TAG, "Error parsing protocols");
                if (BuildConfig.TRACE) {
                    e.printStackTrace();
                }
            }
            switch (serverObj) {
                case HTTPS:
                    protocolEnums.add(ProtocolEnum.HTTPS);
                    break;
                case BOLINA:
                    protocolEnums.add(ProtocolEnum.BOLINA);
                    break;
                case HTTP:
                    protocolEnums.add(ProtocolEnum.HTTP);
                    break;
                default:
                    Log.e(TAG, "Unknown protocol: " + serverObj);
                    break;
            }
        }
    }

    /**
     * Callback indicating a file was created by async task
     *
     * @param message message from async creation
     */
    @Override
    public void processFinish(Object message) {

        Log.d(TAG, (String) message);
    }
}

