/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.protocol;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.domain.ContentServer;
import com.codavel.bolina.demo.domain.ModeEnum;
import com.codavel.bolina.demo.domain.ServerFile;
import com.codavel.bolina.demo.domain.utils.CommonObjects;
import com.codavel.bolina.demo.ui.fragments.FragmentHome;
import com.codavel.bolina.interceptor.okhttp3.CvlOkHttp3Interceptor;


import java.io.File;
import java.io.IOException;
import java.io.Reader;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.CacheControl;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Util;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.ForwardingSource;
import okio.Okio;
import okio.Source;

/**
 * Protocol implementation to use Bolina for handle with Downlaod/Upload transfers to a given
 * ContentServer.
 */
public class BolinaProtocol implements ProtocolInterface {
    /**
     * Tag for logging
     */
    private static final String TAG = AppData._TAG + "[BOLINA-PROTO]";
    /**
     * Scheme used by requests
     */
    private static final String SCHEME = "https://";
    /**
     * Message Handler to update the UI
     */
    private Handler toActivity;
    /**
     * The frequency in milliseconds to update UI
     */
    private static final int UPDATE_UI_FREQUENCY = 100;
    /**
     * Keys used on HTTP headers
     */
    private static final String HEADER_KEY = "Content-Disposition";
    private static final String HEADER_VALUE_START = "form-data; name=\"image\"; filename=\"";
    private static final String HEADER_VALUE_END = "\"";
    private static final String MIME_TYPE = "image/*";
    private static final String PATH_SEPARATOR = "/";
    private static final String BOLINA_ADDRESS_KEY = "Bolina-Address";
    /**
     * Separator used to separate server name or IP from server port
     */
    private static final String SEPARATOR = ":";
    /**
     * MESSAGE objects
     */
    private static final String BYTES_READ = "bytes_read";
    /**
     * Time in milliseconds since last update on UI
     */
    private long lastUpdate = 0;
    /**
     * Counter with sum of bytes read. This value is accumulated until a update on the UI is called,
     * then we restart it to 0 until we reach the end of transfer.
     */
    private long totalRead = 0;
    /**
     * Okhttp instance for download
     */
    private static OkHttpInstance downloadSingleton;
    /**
     * Okhttp instance for upload
     */
    private static OkHttpInstance uploadSingleton;
    /**
     * Locks if OkHttpInstance is under construction
     */
    private static final Object lock = new Object();

    /**
     * Public getter for the singleton instance
     *
     * @return OkHttp client instance
     */
    private OkHttpInstance getDownloadInstance() {

        synchronized (lock) {
            if (downloadSingleton == null) {
                //create a listener to receive updates on progress
                final ProgressListener progressListener = new ProgressListener() {

                    @Override
                    public void update(long bytesRead, long contentLength, boolean done) {

                        totalRead += bytesRead;

                        if (done || System.currentTimeMillis() - lastUpdate > UPDATE_UI_FREQUENCY) {
                            updateUI(FragmentHome.DOWNLOAD_UPDATE, totalRead);
                            totalRead = 0;
                            lastUpdate = System.currentTimeMillis();
                        }
                    }
                };

                //creating interceptor with the listener created before
                Interceptor interceptor = new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Response originalResponse = chain.proceed(chain.request());
                        if (BuildConfig.TRACE) {
                            Log.i(TAG, "response code " + originalResponse.code());
                        }
                        return originalResponse.newBuilder()
                                .body(new ProgressResponseBody(originalResponse.body(),
                                        progressListener))
                                .build();
                    }
                };

                ArrayList<Interceptor> interceptors = new ArrayList<Interceptor>();
                interceptors.add(interceptor);
                interceptors.add(new CvlOkHttp3Interceptor());

                downloadSingleton = new OkHttpInstance(interceptors);
            }
        }

        return downloadSingleton;
    }

    /**
     * Public getter for the singleton instance
     *
     * @return OkHttp client instance
     */
    private OkHttpInstance getUploadInstance() {

        synchronized (lock) {
            if (uploadSingleton == null) {
                ArrayList<Interceptor> interceptors = new ArrayList<Interceptor>();
                interceptors.add(new CvlOkHttp3Interceptor());
                uploadSingleton = new OkHttpInstance(interceptors);
            }
        }

        return uploadSingleton;
    }

    /**
     * Performs the given Trial using HTTPS protocol implementation.
     *
     * @param mode          Trial ModeEnum;
     * @param contentServer ContentServer of the trial;
     * @param serverFile    ServerFile of the Trial;
     * @param toActivity    communication handler to UI activity;
     */
    @Override
    public void performTrial(ModeEnum mode, ContentServer contentServer, ServerFile serverFile,
                             Handler toActivity) {

        this.toActivity = toActivity;

        switch (mode) {
            case GET: {
                runDownload(contentServer.getAddress(), SEPARATOR + contentServer.
                        getServerPort() + serverFile.getFilePath());
                break;
            }
            case POST: {
                runUpload(contentServer.getAddress(), SEPARATOR + contentServer.
                        getServerPort() + serverFile.getUploadPath(), serverFile);
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * Performs the Trial download.
     *
     * @param server  server address
     * @param urlPath ContentServer address url for the resource;
     */
    private void runDownload(String server, String urlPath) {

        updateUI(FragmentHome.DOWNLOAD_START, 0);

        long start_time = System.currentTimeMillis();
        lastUpdate = System.currentTimeMillis();

        Log.i(TAG, "Starting download of " + SCHEME + server + urlPath);

        //build okhttp request
        Request request = new Request.Builder()
                .url(SCHEME + server + urlPath)
                .cacheControl(new CacheControl.Builder().noCache().build())
                .addHeader(BOLINA_ADDRESS_KEY, server)
                .build();

        OkHttpClient client = getDownloadInstance().getClient();

        Response response = null;
        Reader reader = null;
        try {
            response = client.newCall(request).execute();
            response.sentRequestAtMillis();
            response.receivedResponseAtMillis();
            if (!response.isSuccessful()) {
                response.close();
                if (response.code() == 550) {
                    Log.e(TAG, "Failed to connect to Bolina Server. Check your configurations");
                }
                throw new IOException("Unexpected code " + response);
            }

            //consuming the data
            int read = 0;
            long total = 0;
            char buffer[] = new char[64 * 1024];
            reader = response.body().charStream();
            while (read != -1) {
                read = reader.read(buffer);
                total += read;
            }
            if (BuildConfig.TRACE) {
                double rate = CommonObjects.calculateRate((System.currentTimeMillis() - start_time),
                        response.body().contentLength());
                Log.d(TAG, "Completed download of " + request.url().toString() + " (" +
                        response.body().contentLength() + "): " + rate + " Mbps");
            }
        } catch (SocketTimeoutException e) {
            Log.e(TAG, "Download timeout");
        } catch (IOException e) {
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            }
        } catch (NullPointerException e) {
            Log.e(TAG,
                    "Failed to connect to Bolina Server. Check your configurations");
        } catch (Exception e) {
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            } else {
                Log.e(TAG, e.getMessage());
            }
        } finally {
            try {
                if (reader != null) {
                    reader.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                if (BuildConfig.TRACE) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Performs the Trial upload.
     *
     * @param server     server address
     * @param urlPath    ContentServer address url to upload the file;
     * @param serverFile ServerFile to be uploaded;
     */
    private void runUpload(String server, String urlPath, ServerFile serverFile) {

        updateUI(FragmentHome.UPLOAD_START, 0);

        Log.i(TAG, "Starting upload of " + serverFile.getFileName() + " to " + SCHEME +
                server + urlPath);

        lastUpdate = System.currentTimeMillis();
        long start_time = System.currentTimeMillis();

        RequestBody requestBody = null;

        if (!CommonObjects.USE_RANDOM_BYTES) {
            final File file = new File(FragmentHome.getDirPath() + PATH_SEPARATOR +
                    serverFile.getFileName());

            requestBody = new MultipartBody.Builder()
                    .addPart(
                            Headers.of(HEADER_KEY, HEADER_VALUE_START +
                                    file.getName() + HEADER_VALUE_END),
                            new CountingFileRequestBody(file, MIME_TYPE,
                                    new CountingFileRequestBody.ListenerProgress() {
                                        @Override
                                        public void transferred(long num) {
                                        }
                                    })).build();
        } else {

            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
            MultipartBody.Builder multipartBody = new MultipartBody.Builder().setType(MultipartBody.
                    FORM);

            //Generating random bytes arrays to upload
            long fileSize = serverFile.getFileSize();
            byte[] data = new byte[64 * 1024];
            long bytestBody = 0;

            while (bytestBody < fileSize) {
                if (bytestBody + data.length > fileSize) {
                    long difference = fileSize - bytestBody;
                    data = new byte[(int) difference];
                    multipartBody.addPart(RequestBody.create(MEDIA_TYPE_PNG, data));
                    bytestBody += data.length;
                } else {
                    multipartBody.addPart(RequestBody.create(MEDIA_TYPE_PNG, data));
                    bytestBody += data.length;
                }
            }
            requestBody = multipartBody.build();
        }

        Request request = new Request.Builder()
                .url(SCHEME + server + urlPath)
                .addHeader(BOLINA_ADDRESS_KEY, server)
                .cacheControl(new CacheControl.Builder().noCache().build())
                .post(requestBody)
                .build();

        OkHttpClient client = getUploadInstance().getClient();

        Response response = null;
        try {
            response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                response.close();
                throw new IOException("Unexpected code " + response);
            }

            if (BuildConfig.TRACE) {
                double rate = CommonObjects.calculateRate((System.currentTimeMillis() - start_time),
                        serverFile.getFileSize());
                Log.d(TAG, "Completed upload of " + request.url().toString() + " (" +
                        serverFile.getFileSize() + "): " + rate + " Mbps");
            }

            updateUI(FragmentHome.UPLOAD_UPDATE, serverFile.getFileSize());
        } catch (NullPointerException e) {
            Log.e(TAG,
                    "Failed to connect to Bolina Server. Check your configurations");
        } catch (Exception e) {
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            } else {
                Log.e(TAG, e.getMessage());
            }
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    /**
     * OkHttp Request Body implementation to get progress updates for upload. This is made using
     * Okio I/O API
     * Retrieved from https://gist.github.com/eduardb/dd2dc530afd37108e1ac.
     */
    private static class CountingFileRequestBody extends RequestBody {

        /**
         * Okio buffer size
         */
        private static final int SEGMENT_SIZE = 60 * 1024;
        /**
         * File to be uploaded
         */
        private final File file;
        /**
         * Progress upload listener
         */
        private final ListenerProgress listener;
        /**
         * Content type
         */
        private final String contentType;

        /**
         * CountingFileRequestBody constructor
         *
         * @param file        file to be uploaded
         * @param contentType files's content-type to be uploaded
         * @param listener    progress listener
         */
        CountingFileRequestBody(File file, String contentType, ListenerProgress listener) {

            this.file = file;
            this.contentType = contentType;
            this.listener = listener;
        }

        /**
         * Getter for file content length
         *
         * @return file content length
         */
        @Override
        public long contentLength() {

            return file.length();
        }

        /**
         * Mime type of data
         *
         * @return mime type of data
         */
        @Override
        public MediaType contentType() {

            return MediaType.parse(contentType);
        }

        /**
         * RequestBody reads the file content to a Okio I/O source
         *
         * @param sink Buffered sink to get file content
         * @throws IOException exception occurred from reading
         */
        @Override
        public void writeTo(BufferedSink sink) throws IOException {

            Source source = null;

            try {
                source = Okio.source(file);
                long read;

                while ((read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                    sink.flush();
                    this.listener.transferred(read);
                }
            } finally {
                Util.closeQuietly(source);
            }
        }

        /**
         * Upload progress listener
         */
        public interface ListenerProgress {

            void transferred(long num);
        }
    }

    /**
     * ResponseBody implementation to intercept the response and call the progress listener
     */
    private static class ProgressResponseBody extends ResponseBody {
        /**
         * ResponseBody instance
         */
        private final ResponseBody responseBody;
        /**
         * Progress download listener
         */
        private final ProgressListener progressListener;
        /**
         * Buffer with content source
         */
        private BufferedSource bufferedSource;

        /**
         * ProgressResponseBody constructor
         *
         * @param responseBody     OkHttp ResponseBody received on Response
         * @param progressListener listener to update the progress of response reading
         */
        ProgressResponseBody(ResponseBody responseBody, ProgressListener
                progressListener) {

            this.responseBody = responseBody;
            this.progressListener = progressListener;
        }

        /**
         * Mime type of data
         *
         * @return mime type of data
         */
        @Override
        public MediaType contentType() {

            return responseBody.contentType();
        }

        /**
         * Getter for file content length
         *
         * @return file content length
         */
        @Override
        public long contentLength() {

            return responseBody.contentLength();
        }

        /**
         * Getter for the Buffered source
         *
         * @return Buffered source
         */
        @Override
        public BufferedSource source() {

            if (bufferedSource == null) {
                bufferedSource = Okio.buffer(source(responseBody.source()));
            }
            return bufferedSource;
        }

        /**
         * Forwards the Source ResponseBody and calls the Listener with the update progress.
         *
         * @param source Okio Source that reads the Response stream
         * @return Okio Source forwarded from input
         */
        private Source source(Source source) {

            return new ForwardingSource(source) {
                long totalBytesRead = 0L;

                @Override
                public long read(Buffer sink, long byteCount) throws IOException {
                    long bytesRead = super.read(sink, byteCount);

                    // read() returns the number of bytes read, or -1 if this source is exhausted.
                    totalBytesRead = bytesRead != -1 ? bytesRead : 0;
                    progressListener.update(totalBytesRead, responseBody.contentLength(),
                            bytesRead == -1);
                    return bytesRead;
                }
            };
        }
    }

    /**
     * Progress ListenerInterface to publish the progress update.
     */
    interface ProgressListener {

        void update(long bytesRead, long contentLength, boolean done);
    }

    /**
     * Updates the UI sending messages through the Hanler.
     *
     * @param messageWhat Message 'what' code
     * @param bytesRead   number of bytes read
     */
    private void updateUI(int messageWhat, long bytesRead) {

        Message message = new Message();
        message.what = messageWhat;
        HashMap<String, Object> messageObject = new HashMap<>();

        //Updates the message what when needed
        switch (messageWhat) {
            case FragmentHome.DOWNLOAD_UPDATE: {
                messageObject.put(BYTES_READ, bytesRead);
                break;
            }
            case FragmentHome.UPLOAD_UPDATE: {
                messageObject.put(BYTES_READ, bytesRead);
                break;
            }
        }
        message.obj = messageObject;
        try {
            if (toActivity != null) {

                //send message
                toActivity.dispatchMessage(message);
            }
        } catch (Exception e) {
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            } else {
                Log.e(TAG, e.getMessage());
            }
        }
    }
}
