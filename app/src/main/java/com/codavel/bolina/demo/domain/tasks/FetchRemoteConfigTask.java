/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.tasks;

import android.content.Context;
import android.os.AsyncTask;
import android.util.Log;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.domain.TestProfile;
import com.codavel.bolina.demo.domain.protocol.OkHttpSharedSingleton;
import com.codavel.bolina.demo.domain.utils.JSONParser;

import org.json.JSONObject;

import java.io.InputStream;
import java.lang.ref.WeakReference;
import java.nio.charset.StandardCharsets;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * An async task to retrieve the configuration settings from a remote content server and transform
 * the response into a Test.
 */
public class FetchRemoteConfigTask extends AsyncTask<Void, Void, TestProfile> {
    /**
     * The client authentication token
     */
    private static final String TOKEN = BuildConfig.AUTH_TOKEN;
    /**
     * The URL for the configuration
     */
    private static final String URL_STRING = BuildConfig.REMOTE_CONFIG;
    /**
     * The name of json config file
     */
    private static final String CONFIG_FILE = "config.json";
    /**
     * TAG for logging on LOG
     */
    private static final String TAG = AppData._TAG + "[FETCH-CONF]";
    /**
     * Authorization
     */
    private static final String AUTHORIZATION = "Authorization";
    /**
     * Delegate to handle the flow after response
     */
    public AsyncResponse delegate = null;
    /**
     * Application context
     */
    private WeakReference<Context> context;

    public FetchRemoteConfigTask(Context applicationContext) {

        this.context = new WeakReference<>(applicationContext);
    }

    /**
     * Create an OkHttpClient and Request to fetch the remote configuration parsing the response
     * into a Test.
     */
    @Override
    protected TestProfile doInBackground(Void... voids) {
        try {

            String jsonData = "{}";
            if (BuildConfig.USE_LOCAL_CONFIG) {
                InputStream is = context.get().getAssets().open(CONFIG_FILE);
                int size = is.available();
                byte[] buffer = new byte[size];
                is.read(buffer);
                is.close();
                jsonData = new String(buffer, StandardCharsets.UTF_8);
            } else {
                OkHttpClient client =
                        OkHttpSharedSingleton.getSharedInstance().newBuilder().build();
                Request request = new Request.Builder()
                        .url(URL_STRING)
                        .addHeader(AUTHORIZATION, TOKEN)
                        .build();
                Response responses = null;

                responses = client.newCall(request).execute();
                jsonData = responses.body().string();

                // force shutdown
                client.dispatcher().executorService().shutdown();
                client.connectionPool().evictAll();
            }
            JSONObject json_object = new JSONObject(jsonData);
            JSONParser parser = new JSONParser(json_object);

            return parser.getTestParsed();

        } catch (Exception e) {
            e.printStackTrace();
            Log.e(TAG, "Error fetching config: " + e.getMessage());
            return new TestProfile();
        }
    }


    /**
     * Calls the delegate handler announcing that the process is finished and a Test was created.
     *
     * @param test Test parsed form the remote config;
     */
    @Override
    protected void onPostExecute(TestProfile test) {

        super.onPostExecute(test);
        delegate.processFinish(test);
    }
}