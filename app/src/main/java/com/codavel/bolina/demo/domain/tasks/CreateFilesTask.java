/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.tasks;

import android.os.AsyncTask;

import com.codavel.bolina.demo.MainActivity;

import java.io.File;
import java.io.FileOutputStream;
import java.util.Random;

/**
 * Create the files required for upload Trials
 */
public class CreateFilesTask extends AsyncTask<Void, Void, String> {

    /**
     * Delegate to handle the flow after response
     */
    public AsyncResponse delegate = null;
    /**
     * Path separator
     */
    private static final String PATH = "/";
    /**
     * File name to be created
     */
    private String fileName;
    /**
     * File size
     */
    private long fileSize;

    /**
     * CreateFilesTask constructor of task to create the file
     *
     * @param fileName file name
     * @param fileSize file size
     */
    public CreateFilesTask(String fileName, long fileSize) {

        this.fileName = fileName;
        this.fileSize = fileSize;
    }


    /**
     * Check if file exist and if do not exist, the file is then created with the specified size
     * and with random data.
     *
     * @param voids parameters
     * @return creation result
     */
    @Override
    protected String doInBackground(Void... voids) {

        String file_path = MainActivity.getFilesPath() + PATH +
                fileName;

        File file = new File(file_path);

        if (file.exists()) {
            return fileName + " already exists";
        } else {
            FileOutputStream output_stream;
            try {
                output_stream = new FileOutputStream(file_path);

                byte[] buffer = new byte[64 * 1024];
                long total_bytes = 0;

                new Random().nextBytes(buffer);
                while (total_bytes < fileSize) {
                    if ((total_bytes + buffer.length) > fileSize) {
                        long dif = fileSize - total_bytes;
                        output_stream.write(buffer, 0, (int) dif);
                        total_bytes += dif;
                    } else {
                        output_stream.write(buffer, 0, buffer.length);
                        total_bytes += buffer.length;
                    }
                }
                output_stream.close();
            } catch (Exception e) {
                e.printStackTrace();
                return "Exception creating " + fileName;
            }
            return fileName + " created";
        }
    }

    /**
     * After create the files notify files creation
     *
     * @param fileName file name created
     */
    @Override
    protected void onPostExecute(String fileName) {

        super.onPostExecute(fileName);
        delegate.processFinish(fileName);
    }
}