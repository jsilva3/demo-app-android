/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.persistence.sql;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.domain.TrialResult;
import com.codavel.bolina.demo.modal.TestResult;

import java.util.ArrayList;

/**
 * Database handler is a SQLite database manager to handle the persistence of Trial results.
 */
public class DatabaseHandler extends SQLiteOpenHelper {
    /**
     * Tag for logging
     */
    private static final String TAG = AppData._TAG + "[DB]";
    /**
     * Database version
     */
    private static final int DATABASE_VERSION = BuildConfig.VERSION_CODE;
    /**
     * Database name
     */
    private static final String DATABASE_NAME = "Codavel";
    /**
     * Name of existing tables in database
     */
    private static final String TABLE_TRIAL_RESULT = "trial_result";

    /**
     * SQL commands
     */
    private static final String CREATE = "CREATE TABLE ";
    private static final String PRIMARY_KEY_RULE = " INTEGER PRIMARY KEY AUTOINCREMENT NOT NULL,";
    private static final String TEXT_SEPARATOR = " TEXT,";
    private static final String TEXT = " TEXT";
    private static final String EQUAL = " = ";
    private static final String FLOAT_SEPARATOR = " FLOAT,";
    private static final String BEGIN = "(";
    private static final String END = ")";
    private static final String SELECT_FROM = "Select * from ";
    private static final String SELECT_ALL = "Select *,";
    private static final String FROM = " from ";
    private static final String GROUP_BY = " group by ";
    private static final String ORDER_BY = " order by ";
    private static final String DESC = " DESC ";
    private static final String SEPARATOR = ",";
    private static final String DROP = "DROP TABLE IF EXISTS ";

    /**
     * TRIAL_RESULT identifier
     */
    private static final String TRIAL_RESULT_ID = "id";
    /**
     * TRIAL_RESULT protocols identifier
     */
    private static final String TRIAL_RESULT_PROTOCOLS_ID = "protocols_id";
    /**
     * TRIAL_RESULT protocol
     */
    private static final String TRIAL_RESULT_PROTOCOL = "protocol";
    /**
     * TRIAL_RESULT content_server address
     */
    private static final String TRIAL_RESULT_SERVER = "server_name";
    /**
     * TRIAL_RESULT content_server display name (City - Country)
     */
    private static final String TRIAL_RESULT_SERVER_CITY = "server_city";
    /**
     * TRIAL_RESULT network SSID
     */
    private static final String TRIAL_RESULT_NETWORK = "network_name";
    /**
     * TRIAL_RESULT download rate
     */
    private static final String TRIAL_RESULT_DOWNLOAD_SPEED = "download_speed";
    /**
     * TRIAL_RESULT upload rate
     */
    private static final String TRIAL_RESULT_UPLOAD_SPEED = "upload_speed";
    /**
     * TRIAL_RESULT ping average RTT
     */
    private static final String TRIAL_RESULT_PING = "ping";
    /**
     * TRIAL_RESULT date
     */
    private static final String TRIAL_RESULT_DATE = "date";
    /**
     * TRIAL_RESULT download rate average
     */
    private static final String TRIAL_RESULT_DWNL_AVG = "AVG(" + TRIAL_RESULT_DOWNLOAD_SPEED + ")";
    /**
     * TRIAL_RESULT upload rate average
     */
    private static final String TRIAL_RESULT_UPL_AVG = "AVG(" + TRIAL_RESULT_UPLOAD_SPEED + ")";
    /**
     * TRIAL_RESULT ping RTT average
     */
    private static final String TRIAL_RESULT_PING_AVG = "AVG(" + TRIAL_RESULT_PING + ")";

    /**
     * Singleton database instance
     */
    private static DatabaseHandler databaseHandler;

    /**
     * Private constructor for DatabaseHandler.
     *
     * @param context application context
     */
    private DatabaseHandler(Context context) {

        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    /**
     * Public getter for DatabaseHandler single instance. If wasn't created before, DatabaseHandler
     * is created given an application context, otherwise returns the instance.
     *
     * @param mContext application context
     * @return DatabaseHandler instance
     */
    public static DatabaseHandler init(Context mContext) {

        if (databaseHandler == null) {
            synchronized (DatabaseHandler.class) {
                if (databaseHandler == null)
                    databaseHandler = new DatabaseHandler(mContext.getApplicationContext());
            }
        }
        return databaseHandler;
    }

    /**
     * Database tables are created.
     *
     * @param db SQLiteDatabase instance
     */
    @Override
    public void onCreate(SQLiteDatabase db) {

        //Query to create table TABLE_TRIAL_RESULT
        String CREATE_RESULTS_TABLE = CREATE + TABLE_TRIAL_RESULT + BEGIN
                + TRIAL_RESULT_ID + PRIMARY_KEY_RULE
                + TRIAL_RESULT_PROTOCOL + TEXT_SEPARATOR
                + TRIAL_RESULT_NETWORK + TEXT_SEPARATOR
                + TRIAL_RESULT_SERVER + TEXT_SEPARATOR
                + TRIAL_RESULT_SERVER_CITY + TEXT_SEPARATOR
                + TRIAL_RESULT_DOWNLOAD_SPEED + FLOAT_SEPARATOR
                + TRIAL_RESULT_UPLOAD_SPEED + FLOAT_SEPARATOR
                + TRIAL_RESULT_PING + FLOAT_SEPARATOR
                + TRIAL_RESULT_PROTOCOLS_ID + TEXT_SEPARATOR
                + TRIAL_RESULT_DATE + TEXT + END;

        //executing the SQL queries to create the tables
        db.execSQL(CREATE_RESULTS_TABLE);
    }

    /**
     * Upgrade will recreate the database tables dropping the existent
     *
     * @param db         SQLiteDatabase instance
     * @param oldVersion old version
     * @param newVersion new version
     */
    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {

        // Drop older table if existed
        db.execSQL(DROP + TABLE_TRIAL_RESULT);

        // Create tables again
        onCreate(db);
    }

    /**
     * Inserts the information of a TrialResult into database.
     *
     * @param trial_result TrialResult with metrics to be persisted
     * @return total number of entries in the TABLE_TRIAL_RESULT
     */
    public long addTrialResult(TrialResult trial_result) {

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();

        values.put(TRIAL_RESULT_SERVER,
                trial_result.getDownloadTrial().getContentServer().toString());
        values.put(TRIAL_RESULT_SERVER_CITY,
                trial_result.getDownloadTrial().getContentServer().getCity());
        values.put(TRIAL_RESULT_NETWORK, trial_result.getDownloadTrial().getNetworkSSID());
        values.put(TRIAL_RESULT_PROTOCOL, trial_result.getDownloadTrial().getProtocol().toString());
        values.put(TRIAL_RESULT_PING, trial_result.getAverageRTT());
        values.put(TRIAL_RESULT_DOWNLOAD_SPEED, trial_result.getDownloadTrial().getTrialRate());
        values.put(TRIAL_RESULT_UPLOAD_SPEED, trial_result.getUploadTrial().getTrialRate());
        values.put(TRIAL_RESULT_DATE, String.valueOf(System.currentTimeMillis()));
        values.put(TRIAL_RESULT_PROTOCOLS_ID, trial_result.getProtocolCompareId());

        long rows = db.update(TABLE_TRIAL_RESULT, values, TRIAL_RESULT_ID + EQUAL
                + 0, null);
        if (rows == 0) {
            rows = db.insert(TABLE_TRIAL_RESULT, null, values);
        }
        return rows;
    }

    /**
     * Fetch all the trials results persisted in database.
     *
     * @return ArrayList of TestResult
     */
    public ArrayList<TestResult> getAllTestResultData() {
        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<TestResult> list = new ArrayList<>();
        TestResult data;
        Cursor cursor = null;
        try {
            cursor = db.rawQuery(SELECT_FROM + TABLE_TRIAL_RESULT + ORDER_BY +
                    TRIAL_RESULT_DATE +SEPARATOR+TRIAL_RESULT_PROTOCOLS_ID+ DESC, null);
            while (cursor.moveToNext()) {
                data = new TestResult();
                data.testId = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_PROTOCOLS_ID));
                data.server = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_SERVER));
                data.city = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_SERVER_CITY));
                data.protocol = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_PROTOCOL));
                data.network = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_NETWORK));
                data.ping = cursor.getFloat(cursor.getColumnIndex(TRIAL_RESULT_PING));
                data.downloadSpeed = cursor.getFloat(
                        cursor.getColumnIndex(TRIAL_RESULT_DOWNLOAD_SPEED));
                data.uploadSpeed = cursor.getFloat(
                        cursor.getColumnIndex(TRIAL_RESULT_UPLOAD_SPEED));
                data.date = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_DATE));

                list.add(data);
            }
            cursor.close();
        } catch (Exception e) {
            Log.e(TAG, "Exception fetching results");
            if (cursor != null) {
                cursor.close();
            }
            return list;
        }
        return list;
    }

    /**
     * Fetch all the trials results persisted in database, aggregated by Protocol and Network SSID.
     *
     * @return ArrayList of TestResult
     */
    public ArrayList<TestResult> getByNetworkAggregation() {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<TestResult> list = new ArrayList<>();
        TestResult data;
        Cursor cursor = null;
        String query = SELECT_ALL + TRIAL_RESULT_PING_AVG + SEPARATOR + TRIAL_RESULT_UPL_AVG +
                SEPARATOR + TRIAL_RESULT_DWNL_AVG + FROM + TABLE_TRIAL_RESULT + GROUP_BY +
                TRIAL_RESULT_NETWORK + SEPARATOR + TRIAL_RESULT_PROTOCOL;
        try {
            cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                data = new TestResult();
                data.testId = String.valueOf(cursor.getInt(cursor.getColumnIndex(TRIAL_RESULT_ID)));
                data.server = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_SERVER));
                data.city = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_SERVER_CITY));
                data.protocol = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_PROTOCOL));
                data.network = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_NETWORK));
                data.ping = cursor.getFloat(cursor.getColumnIndex("AVG(ping)"));
                data.downloadSpeed = cursor.getFloat(
                        cursor.getColumnIndex("AVG(download_speed)"));
                data.uploadSpeed = cursor.getFloat(
                        cursor.getColumnIndex("AVG(upload_speed)"));
                data.date = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_DATE));

                list.add(data);
            }
            cursor.close();
        } catch (Exception e) {
            Log.e(TAG, "Exception fetching results");
            if (cursor != null) {
                cursor.close();
            }
        }
        return list;
    }

    /**
     * Fetch all the trials results persisted in database, aggregated by Protocol and Server name.
     *
     * @return ArrayList of TestResult
     */
    public ArrayList<TestResult> getByServerAggregation() {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<TestResult> list = new ArrayList<>();
        TestResult data;
        Cursor cursor = null;
        String query = SELECT_ALL + TRIAL_RESULT_PING_AVG + SEPARATOR + TRIAL_RESULT_UPL_AVG +
                SEPARATOR + TRIAL_RESULT_DWNL_AVG + FROM + TABLE_TRIAL_RESULT + GROUP_BY +
                TRIAL_RESULT_SERVER + SEPARATOR + TRIAL_RESULT_PROTOCOL;
        try {
            cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                data = new TestResult();
                data.testId = String.valueOf(cursor.getInt(cursor.getColumnIndex(TRIAL_RESULT_PROTOCOLS_ID)));
                data.server = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_SERVER));
                data.city = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_SERVER_CITY));
                data.protocol = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_PROTOCOL));
                data.network = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_NETWORK));
                data.ping = cursor.getFloat(cursor.getColumnIndex(TRIAL_RESULT_PING_AVG));
                data.downloadSpeed = cursor.getFloat(
                        cursor.getColumnIndex(TRIAL_RESULT_DWNL_AVG));
                data.uploadSpeed = cursor.getFloat(
                        cursor.getColumnIndex(TRIAL_RESULT_UPL_AVG));
                data.date = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_DATE));

                list.add(data);
            }
            cursor.close();
        } catch (Exception e) {
            Log.e(TAG, "Exception fetching results");
            if (cursor != null) {
                cursor.close();
            }
        }
        return list;
    }

    /**
     * Fetch all the trials results persisted in database, aggregated by Protocol, Network SSID and
     * Server name.
     *
     * @return ArrayList of TestResult
     */
    public ArrayList<TestResult> getByServerAndNetworkAggregation() {

        SQLiteDatabase db = this.getReadableDatabase();
        ArrayList<TestResult> list = new ArrayList<>();
        TestResult data;
        Cursor cursor = null;
        String query = SELECT_ALL + TRIAL_RESULT_PING_AVG + SEPARATOR + TRIAL_RESULT_UPL_AVG +
                SEPARATOR + TRIAL_RESULT_DWNL_AVG + FROM + TABLE_TRIAL_RESULT + GROUP_BY +
                TRIAL_RESULT_SERVER + SEPARATOR + TRIAL_RESULT_PROTOCOL + SEPARATOR +
                TRIAL_RESULT_NETWORK + ORDER_BY + TRIAL_RESULT_PROTOCOLS_ID ;
        try {
            cursor = db.rawQuery(query, null);
            while (cursor.moveToNext()) {
                data = new TestResult();
                data.testId = String.valueOf(cursor.getInt(cursor.getColumnIndex(TRIAL_RESULT_ID)));
                data.server = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_SERVER));
                data.city = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_SERVER_CITY));
                data.protocol = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_PROTOCOL));
                data.network = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_NETWORK));
                data.ping = cursor.getFloat(cursor.getColumnIndex(TRIAL_RESULT_PING_AVG));
                data.downloadSpeed = cursor.getFloat(
                        cursor.getColumnIndex(TRIAL_RESULT_DWNL_AVG));
                data.uploadSpeed = cursor.getFloat(
                        cursor.getColumnIndex(TRIAL_RESULT_UPL_AVG));
                data.date = cursor.getString(cursor.getColumnIndex(TRIAL_RESULT_DATE));

                list.add(data);
            }
            cursor.close();
        } catch (Exception e) {
            Log.e(TAG, "Exception fetching results");
            if (cursor != null) {
                cursor.close();
            }
        }
        return list;
    }
}