/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;
import android.widget.Toast;

import com.codavel.bolina.demo.domain.utils.CommonObjects;

import butterknife.ButterKnife;

/**
 * Activity to request needed permissions
 */
public class SplashActivity extends AppCompatActivity {
    /**
     * Permissions request code for broadcast receiver
     */
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 101;
    /**
     * Message to pass on Toast on missing permissions
     */
    private static final String PERMISSION_MESSAGE = "Without permissions we cannot test uploads!";
    /**
     * Application context
     */
    private Context mContext;
    /**
     * List of needed permissions
     */
    private static final String[] PERMISSIONS = {
            Manifest.permission.ACCESS_COARSE_LOCATION,
            Manifest.permission.ACCESS_FINE_LOCATION,
            Manifest.permission.READ_EXTERNAL_STORAGE,
            Manifest.permission.WRITE_EXTERNAL_STORAGE
    };

    /**
     * @param savedInstanceState
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        this.requestWindowFeature(Window.FEATURE_NO_TITLE);

        setContentView(R.layout.activity_splash);
        getSupportActionBar().hide();
        mContext = this;
        ButterKnife.bind(this);
    }

    /**
     * Receives the request permissions results
     *
     * @param requestCode  request code of response
     * @param permissions  array with permissions
     * @param grantResults array with results
     */
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String permissions[],
                                           @NonNull int[] grantResults) {

        switch (requestCode) {
            case REQUEST_PERMISSIONS_REQUEST_CODE: {

                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0
                        && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    CommonObjects.CAN_TEST_UPLOAD = true;
                } else {
                    Toast.makeText(this, PERMISSION_MESSAGE,
                            Toast.LENGTH_SHORT).show();
                }
                startActivity(new Intent(mContext, MainActivity.class));
                finish();
            }
        }
    }

    /**
     * Check if user already gave permissions
     *
     * @param context     application context
     * @param permissions list of permissions
     * @return true if user gave permissions otherwise false
     */
    public static boolean hasPermissions(Context context, String... permissions) {

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M && context != null && permissions !=
                null) {
            for (String permission : permissions) {
                if (ActivityCompat.checkSelfPermission(context, permission) !=
                        PackageManager.PERMISSION_GRANTED) {
                    return false;
                }
            }
        }
        return true;
    }

    /**
     * Asks permissions on start
     */
    @Override
    protected void onStart() {

        super.onStart();
        if (!hasPermissions(mContext, PERMISSIONS)) {
            ActivityCompat.requestPermissions(this, PERMISSIONS,
                    REQUEST_PERMISSIONS_REQUEST_CODE);
        } else {
            CommonObjects.CAN_TEST_UPLOAD = true;
            startActivity(new Intent(mContext, MainActivity.class));
            finish();
        }
    }
}