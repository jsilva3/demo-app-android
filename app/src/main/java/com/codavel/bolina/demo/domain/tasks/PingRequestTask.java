/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.tasks;

import android.os.AsyncTask;
import android.util.Log;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.domain.protocol.OkHttpSharedSingleton;

import java.io.IOException;
import java.security.cert.CertificateException;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

/**
 * To measure RTT between client and server we use this task to perform several small HTTP requests
 * (10B) keeping the lowest value as RTT
 */
public class PingRequestTask extends AsyncTask<Void, Void, Integer> {
    private static final String SSL = "SSL";

    /**
     * TAG for logging on LOG
     */
    private static final String TAG = AppData._TAG + "[PING]";
    /**
     * URL scheme
     */
    private static final String SCHEME = "https://";
    /**
     * Number of requests to perform
     */
    private static final int N_ATTEMPTS = 10;

    /**
     * Delegate to handle the flow after response
     */
    public AsyncResponse delegate = null;
    /**
     * Okhttp client instance
     */
    private OkHttpClient client;
    /**
     * Http request
     */
    private Request request;
    /**
     * Start value for elapsed time
     */
    private int elapsedTime = 0;

    /**
     * Public constructor, build the Okhttp client and request to be performed
     *
     * @param url content server url to be fetched
     */
    public PingRequestTask(String url) {

        client = getUnsafeOkHttpClient();
        request = new Request.Builder()
                .url(SCHEME + url)
                .build();
    }

    /**
     * Performs N_ATTEMPTS requests in background
     *
     * @param voids parameters
     * @return time spent
     */
    @Override
    protected Integer doInBackground(Void... voids) {
        if (BuildConfig.TRACE) {
            Log.d(TAG, "Ping URL:" + request.url().toString());
        }
        for (int i = 0; i < N_ATTEMPTS; i++) {
            try {
                run();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        // force shutdown
        client.dispatcher().executorService().shutdown();
        client.connectionPool().evictAll();

        return elapsedTime;
    }

    /**
     * Performs the HTTP requests
     */
    public void run() {

        final long startTime = System.currentTimeMillis();

        try {
            Response response = client.newCall(request).execute();
            if (!response.isSuccessful()) {
                response.close();
                throw new IOException("Unexpected code " + response);
            }

            checkTimeResult(System.currentTimeMillis() - startTime);
            response.close();
        } catch (Exception e) {
            if (BuildConfig.TRACE) {
                Log.e(TAG, "Exception on Ping task");
                e.printStackTrace();
            }
        }
    }

    /**
     * Checks the result times, analyzing if the time was lower than the lowest registered and
     * keeping the lowest registered time for future analysis
     *
     * @param elapsedTime time elpased on the latest request
     */
    private void checkTimeResult(long elapsedTime) {
        if (BuildConfig.TRACE) {
            Log.i(TAG, "Ping: " + elapsedTime);
        }

        if (this.elapsedTime == 0) {
            this.elapsedTime = (int) elapsedTime;
        } else {
            if (elapsedTime < this.elapsedTime) {
                this.elapsedTime = (int) elapsedTime;
                if (BuildConfig.TRACE) {
                    Log.i(TAG, "Lowest ping time! " + elapsedTime);
                }
            }
        }
    }

    /**
     * After complete notifies the delegate with the result
     *
     * @param result RTT time to the content server
     */
    @Override
    protected void onPostExecute(Integer result) {

        super.onPostExecute(result);
        delegate.processFinish(result);
    }

    /**
     * Creates a instance of Okhttp client accepting all the certs without validation
     *
     * @return Okhttp client instance
     */
    private OkHttpClient getUnsafeOkHttpClient() {

        try {

            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                                       String authType) throws
                                CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                                       String authType) throws
                                CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance(SSL);
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = OkHttpSharedSingleton.getSharedInstance().newBuilder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {

                    return true;
                }
            });

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}
