/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.protocol;

import java.security.cert.CertificateException;
import java.util.ArrayList;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.X509TrustManager;

import okhttp3.Interceptor;
import okhttp3.OkHttpClient;

/**
 * Single instantiation of OkHttp clients
 */
public class OkHttpInstance {
    private static final String SSL = "SSL";

    /**
     * OkHttp client
     */
    private OkHttpClient client;

    /**
     * Singleton constructor
     *
     * @param interceptors list of OkHttp Interceptors
     */
    OkHttpInstance(ArrayList<Interceptor> interceptors) {

        client = getUnsafeOkHttpClient(interceptors);
    }

    /**
     * Public getter for the client
     *
     * @return OkHttp Client
     */
    OkHttpClient getClient() {

        return client;
    }

    /**
     * Create insecure instance of OkHttpClient. This will disable the validation of the Host Name
     * Verifier and allows all SSL certificates.
     *
     * @param interceptors list of OkHttp Interceptors
     * @return instance of OkHttpClient
     */
    private OkHttpClient getUnsafeOkHttpClient(ArrayList<Interceptor> interceptors) {

        try {

            // Create a trust manager that does not validate certificate chains
            final TrustManager[] trustAllCerts = new TrustManager[]{
                    new X509TrustManager() {
                        @Override
                        public void checkClientTrusted(java.security.cert.X509Certificate[] chain,
                                                       String authType) throws
                                CertificateException {
                        }

                        @Override
                        public void checkServerTrusted(java.security.cert.X509Certificate[] chain,
                                                       String authType) throws
                                CertificateException {
                        }

                        @Override
                        public java.security.cert.X509Certificate[] getAcceptedIssuers() {

                            return new java.security.cert.X509Certificate[]{};
                        }
                    }
            };

            // Install the all-trusting trust manager
            final SSLContext sslContext = SSLContext.getInstance(SSL);
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());

            // Create an ssl socket factory with our all-trusting manager
            final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();

            OkHttpClient.Builder builder = OkHttpSharedSingleton.getSharedInstance().newBuilder();
            builder.sslSocketFactory(sslSocketFactory, (X509TrustManager) trustAllCerts[0]);
            builder.hostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {

                    return true;
                }
            });

            // Adding the interceptor if passed
            if (interceptors != null) {
                for (Interceptor interceptor : interceptors) {
                    builder.addInterceptor(interceptor);
                }
            }

            return builder.build();
        } catch (Exception e) {
            throw new RuntimeException(e);
        }
    }
}