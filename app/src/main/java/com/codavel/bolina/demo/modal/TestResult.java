/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.modal;

import android.content.Context;
import android.os.Parcel;
import android.os.Parcelable;

import com.codavel.bolina.demo.R;
import com.codavel.bolina.demo.ui.interfaces.ListItem;

import java.util.Comparator;

/**
 * Test result model of a single trial persisted in database, composed to fit the need of UI in
 * aggregated tabs.
 */
public class TestResult implements Parcelable, ListItem, Comparator<TestResult> {

    /**
     * Unique identifier
     */
    public String testId;
    /**
     * Server info
     */
    public String server;
    /**
     * Server city
     */
    public String city;
    /**
     * Protocol used
     */
    public String protocol;
    /**
     * Network SSID
     */
    public String network;
    /**
     * Timestamp date
     */
    public String date;
    /**
     * Download speed
     */
    public double downloadSpeed;
    /**
     * Upload speed
     */
    public double uploadSpeed;
    /**
     * Ping RTT measured
     */
    public double ping;
    /**
     *
     */
    public int getTypes;
    /**
     * Data
     */
    public TestResult data;
    /**
     * Aggregation types
     */
    public String aggTypes;
    /**
     * Application context
     */
    public Context mContext;

    /**
     * Empty constructor
     */
    public TestResult() {
    }

    /**
     * Complete constructor
     *
     * @param server        server info
     * @param protocol      protocol used
     * @param network       network ssid
     * @param date          timestamp date
     * @param downloadSpeed download rates
     * @param uploadSpeed   upload rates
     * @param ping          ping rtt
     */
    public TestResult(String server, String protocol, String network, String date, double
            downloadSpeed, double uploadSpeed, double ping) {

        this.server = server;
        this.protocol = protocol;
        this.network = network;
        this.date = date;
        this.downloadSpeed = downloadSpeed;
        this.uploadSpeed = uploadSpeed;
        this.ping = ping;
    }

    /**
     * Clone constructor
     *
     * @param in Parcel to be cloned
     */
    protected TestResult(Parcel in) {

        server = in.readString();
        city = in.readString();
        protocol = in.readString();
        network = in.readString();
        date = in.readString();
        downloadSpeed = in.readDouble();
        uploadSpeed = in.readDouble();
        ping = in.readDouble();
        getTypes = in.readInt();
        data = in.readParcelable(TestResult.class.getClassLoader());
    }

    /**
     * Creator implementation
     */
    public static final Creator<TestResult> CREATOR = new Creator<TestResult>() {

        @Override
        public TestResult createFromParcel(Parcel in) {
            return new TestResult(in);
        }

        @Override
        public TestResult[] newArray(int size) {
            return new TestResult[size];
        }
    };

    @Override
    public int getType() {

        return TYPE_ITEM;
    }

    @Override
    public int describeContents() {

        return 0;
    }

    @Override
    public void writeToParcel(Parcel parcel, int i) {

        parcel.writeString(server);
        parcel.writeString(city);
        parcel.writeString(protocol);
        parcel.writeString(network);
        parcel.writeString(date);
        parcel.writeDouble(downloadSpeed);
        parcel.writeDouble(uploadSpeed);
        parcel.writeDouble(ping);
        parcel.writeInt(getTypes);
        parcel.writeParcelable(data, i);
    }

    /**
     * Compares two instances of TestResult
     *
     * @param testResult  test to be compared
     * @param testResult1 test to be compared
     * @return
     */
    @Override
    public int compare(TestResult testResult, TestResult testResult1) {

        if (testResult.aggTypes.equalsIgnoreCase(mContext.getResources().getString(R.string.server))) {
            int count = testResult.server.compareTo(testResult1.server);
            return (count != 0) ? count : testResult.server.compareTo(testResult1.server);
        } else if (testResult.aggTypes.equalsIgnoreCase(mContext.getResources().getString(R.string.network))) {
            int count = testResult.network.compareTo(testResult1.network);
            return (count != 0) ? count : testResult.protocol.compareTo(testResult1.protocol);
        } else {
            int count = testResult.network.compareTo(testResult1.network);
            return (count != 0) ? count : testResult.server.compareTo(testResult1.server);
        }
    }
}
