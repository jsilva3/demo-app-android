/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.adapters;

import android.annotation.SuppressLint;
import android.content.Context;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.codavel.bolina.demo.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Tab Adapter to handle the state of Aggregation Fragments (Network, Server and Both)
 */
public class TabAdapter extends FragmentStatePagerAdapter {
    /**
     * List of Fragments
     */
    private final List<Fragment> mFragmentList = new ArrayList<>();
    /**
     * List of Fragments names
     */
    private final List<String> mFragmentTitleList = new ArrayList<>();

    /**
     * TabAdapter constructor
     *
     * @param fragmentManager Fragment Manager
     */
    public TabAdapter(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    /**
     * Getter for Fragment in given position
     *
     * @param position position with intended Fragment
     * @return Fragment in given position
     */
    @Override
    public Fragment getItem(int position) {

        return mFragmentList.get(position);
    }

    /**
     * Adds the fragment and respective name to the lists
     *
     * @param fragment Fragment to be added
     * @param title    Fragment name title
     */
    public void addFragment(Fragment fragment, String title) {

        mFragmentList.add(fragment);
        mFragmentTitleList.add(title);
    }

    /**
     * Getter for page title
     *
     * @param position position of title within the list
     * @return page title
     */
    @Nullable
    @Override
    public CharSequence getPageTitle(int position) {

        return mFragmentTitleList.get(position);
    }

    /**
     * Getter for number of Fragments
     *
     * @return number of Fragments
     */
    @Override
    public int getCount() {

        return mFragmentList.size();
    }

    /**
     * Getter for a view in a given position
     *
     * @param position View's position
     * @param context  application context
     * @return View
     */
    public View getTabView(int position, Context context) {

        @SuppressLint("InflateParams") View view = LayoutInflater.from(context).inflate(R.layout.
                custom_tab, null);
        TextView tabTextView = view.findViewById(R.id.tabTextView);
        tabTextView.setText(mFragmentTitleList.get(position));
        return view;
    }

    /**
     * Getter for the selected View
     *
     * @param position View's position
     * @param context  application context
     * @return View
     */
    public View getSelectedTabView(int position, Context context) {

        @SuppressLint("InflateParams") View view = LayoutInflater.from(context).inflate(
                R.layout.custom_tab, null);
        TextView tabTextView = view.findViewById(R.id.tabTextView);
        tabTextView.setText(mFragmentTitleList.get(position));
        tabTextView.setTextSize(16); // for big text, increase text size
        tabTextView.setTextColor(ContextCompat.getColor(context, R.color.colorPrimaryDark));
        return view;
    }

}
