/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain;

import java.io.Serializable;

/**
 * Enumerator for Mode of a test:
 * - GET   : will run only Downloads
 * - POST  : will run only Uploads
 * - BOTH  : run Downloads and Uploads
 */
public enum ModeEnum implements Serializable {
    POST {
        @Override
        public String toString() {
            return "POST";
        }
    },
    GET {
        @Override
        public String toString() {
            return "GET";
        }
    },
    BOTH {
        @Override
        public String toString() {
            return "BOTH";
        }
    },
}
