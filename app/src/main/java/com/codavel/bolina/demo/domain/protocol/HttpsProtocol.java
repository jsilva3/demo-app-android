/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.protocol;

import android.os.Handler;
import android.os.Message;
import android.util.Log;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.domain.ContentServer;
import com.codavel.bolina.demo.domain.ModeEnum;
import com.codavel.bolina.demo.domain.ServerFile;
import com.codavel.bolina.demo.domain.utils.CommonObjects;
import com.codavel.bolina.demo.ui.fragments.FragmentHome;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.HashMap;

import okhttp3.CacheControl;
import okhttp3.Headers;
import okhttp3.Interceptor;
import okhttp3.MediaType;
import okhttp3.MultipartBody;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;
import okhttp3.ResponseBody;
import okhttp3.internal.Util;
import okio.Buffer;
import okio.BufferedSink;
import okio.BufferedSource;
import okio.ForwardingSource;
import okio.Okio;
import okio.Source;

/**
 * Protocol implementation to use HTTPS for handle with Download/Upload transfers to a given
 * ContentServer.
 */
public class HttpsProtocol implements ProtocolInterface {
    /**
     * Tag for logging
     */
    private static final String TAG = AppData._TAG + "[HTTPS-PROTO]";
    /**
     * Message Handler to update the UI
     */
    private Handler toActivity;
    /**
     * The frequency in milliseconds
     */
    private static final int UPDATE_UI_FREQUENCY = 100;
    /**
     * Header request keys and values
     */
    private static final String HEADER_KEY = "Content-Disposition";
    private static final String HEADER_VALUE_START = "form-data; name=\"image\"; filename=\"";
    private static final String HEADER_VALUE_END = "\"";
    private static final String MIME_TYPE = "image/*";
    private static final String PATH_SEPARATOR = "/";
    /**
     * Separator used to separate server namr or IP from server port
     */
    private static final String SEPARATOR = ":";
    /**
     * MESSAGE objects
     */
    private static final String BYTES_READ = "bytes_read";
    /**
     * Scheme used by requests
     */
    private static final String SCHEME = "https://";
    /**
     * Time in milliseconds since last update on UI
     */
    private long lastUpdate = 0;
    /**
     * Counter with sum of bytes read. This value is accumulated until a update on the UI is called,
     * then we restart it to 0 until we reach the end of transfer.
     */
    private long totalRead = 0;
    /**
     * Okhttp instance for download
     */
    private static OkHttpInstance downloadSingleton;
    /**
     * Okhttp instance for upload
     */
    private static OkHttpInstance uploadSingleton;
    /**
     * Locks if OkHttpInstance is under construction
     */
    private static final Object lock = new Object();

    /**
     * Public getter for the singleton instance
     *
     * @return OkHttp client instance
     */
    private OkHttpInstance getDownloadInstance() {

        synchronized (lock) {
            if (downloadSingleton == null) {
                //create a listener to receive updates on progress
                final ProgressListener progressListener = new ProgressListener() {

                    @Override
                    public void update(long bytesRead, long contentLength, boolean done) {

                        totalRead += bytesRead;

                        if (done || System.currentTimeMillis() - lastUpdate > UPDATE_UI_FREQUENCY) {
                            updateUI(FragmentHome.DOWNLOAD_UPDATE, totalRead);
                            totalRead = 0;
                            lastUpdate = System.currentTimeMillis();
                        }
                    }
                };

                //creating interceptor with the listener created before
                Interceptor interceptor = new Interceptor() {
                    @Override
                    public Response intercept(Chain chain) throws IOException {
                        Response originalResponse = chain.proceed(chain.request());

                        return originalResponse.newBuilder()
                                .body(new ProgressResponseBody(originalResponse.body(),
                                        progressListener))
                                .build();
                    }
                };
                ArrayList<Interceptor> interceptors = new ArrayList<>();
                interceptors.add(interceptor);

                downloadSingleton = new OkHttpInstance(interceptors);
            }
        }

        return downloadSingleton;
    }


    /**
     * Public getter for the singleton instance
     *
     * @return OkHttp client instance
     */
    private OkHttpInstance getUploadInstance() {

        synchronized (lock) {
            if (uploadSingleton == null) {
                uploadSingleton = new OkHttpInstance(null);
            }
        }

        return uploadSingleton;
    }

    /**
     * Performs the given Trial using HTTPS protocol implementation.
     *
     * @param mode          Trial ModeEnum;
     * @param contentServer ContentServer of the trial;
     * @param serverFile    ServerFile of the Trial;
     * @param toActivity    communication handler to UI activity;
     */
    @Override
    public void performTrial(ModeEnum mode, ContentServer contentServer, ServerFile serverFile,
                             Handler toActivity) {

        this.toActivity = toActivity;

        switch (mode) {
            case GET: {
                runDownload(contentServer.getAddress() + SEPARATOR +
                        contentServer.getServerPort() + serverFile.getFilePath());
                break;
            }
            case POST: {
                String url = contentServer.getAddress() + SEPARATOR + contentServer.getServerPort()
                        + serverFile.getUploadPath();

                runUpload(url, serverFile);
                break;
            }
            default: {
                break;
            }
        }
    }

    /**
     * Performs the Trial download.
     *
     * @param urlPath ContentServer address url for the resource;
     */
    private void runDownload(String urlPath) {

        updateUI(FragmentHome.DOWNLOAD_START, 0);

        Log.i(TAG, "Starting download of " + SCHEME + urlPath);

        lastUpdate = System.currentTimeMillis();
        long start_time = System.currentTimeMillis();

        //build okhttp request
        Request request = new Request.Builder()
                .url(SCHEME + urlPath)
                .cacheControl(new CacheControl.Builder().noCache().build())
                .build();

        OkHttpClient client = getDownloadInstance().getClient();

        Response response = null;
        InputStream is = null;
        BufferedInputStream input = null;
        try {
            response = client.newCall(request).execute();

            if (!response.isSuccessful()) {
                response.close();
                throw new IOException("Unexpected code " + response);
            }

            is = response.body().byteStream();
            input = new BufferedInputStream(is);

            byte[] data = new byte[64 * 1024];
            long total = 0, count;

            while ((count = input.read(data)) != -1) {
                total += count;
            }

            if (BuildConfig.TRACE) {
                double rate = CommonObjects.calculateRate((System.currentTimeMillis() - start_time),
                        response.body().contentLength());
                Log.d(TAG, "Completed download of " + request.url().toString() + " (" +
                        response.body().contentLength() + "): " + rate + " Mbps");
            }
        } catch (Exception e) {
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            } else {
                Log.e(TAG, e.getMessage());
            }
        } finally {
            try {
                if (is != null) {
                    is.close();
                }
                if (input != null) {
                    input.close();
                }
                if (response != null) {
                    response.close();
                }
            } catch (IOException e) {
                if (BuildConfig.TRACE) {
                    e.printStackTrace();
                }
            }
        }
    }

    /**
     * Performs the Trial upload.
     *
     * @param urlPath    ContentServer address url to upload the file;
     * @param serverFile ServerFile to be uploaded;
     */
    private void runUpload(String urlPath, ServerFile serverFile) {
        Log.i(TAG, "Starting upload of " + SCHEME + urlPath + serverFile.getFileName());

        lastUpdate = System.currentTimeMillis();
        long start_time = System.currentTimeMillis();

        RequestBody requestBody = null;

        if (!CommonObjects.USE_RANDOM_BYTES) {
            final File file = new File(FragmentHome.getDirPath() + PATH_SEPARATOR +
                    serverFile.getFileName());

            requestBody = new MultipartBody.Builder()
                    .addPart(
                            Headers.of(HEADER_KEY, HEADER_VALUE_START +
                                    file.getName() + HEADER_VALUE_END),
                            new CountingFileRequestBody(file, MIME_TYPE,
                                    new CountingFileRequestBody.ListenerProgress() {
                                        @Override
                                        public void transferred(long num) {
                                        }
                                    })
                    ).build();
        } else {

            final MediaType MEDIA_TYPE_PNG = MediaType.parse("image/png");
            MultipartBody.Builder multipartBody = new MultipartBody.Builder().setType(MultipartBody.FORM);

            //Generating random bytes arrays to upload
            long fileSize = serverFile.getFileSize();
            byte[] data = new byte[64 * 1024];
            long bytestBody = 0;

            while (bytestBody < fileSize) {
                if (bytestBody + data.length > fileSize) {
                    long difference = fileSize - bytestBody;
                    data = new byte[(int) difference];
                    multipartBody.addPart(RequestBody.create(MEDIA_TYPE_PNG, data));
                    bytestBody += data.length;
                } else {
                    multipartBody.addPart(RequestBody.create(MEDIA_TYPE_PNG, data));
                    bytestBody += data.length;
                }
            }
            requestBody = multipartBody.build();
        }

        Request request = new Request.Builder()
                .url(SCHEME + urlPath)
                .cacheControl(new CacheControl.Builder().noCache().build())
                .post(requestBody)
                .build();

        OkHttpClient client = getUploadInstance().getClient();
        Response response = null;
        try {
            response = client.newCall(request).execute();

            if (!response.isSuccessful()) {
                response.close();
                throw new IOException("Unexpected code " + response);
            }

            if (BuildConfig.TRACE) {
                double rate = CommonObjects.calculateRate((System.currentTimeMillis() - start_time),
                        serverFile.getFileSize());
                Log.d(TAG, "Completed upload of " + request.url().toString() + " (" +
                        serverFile.getFileName() + "): " + rate + " Mbps");
            }

            updateUI(FragmentHome.UPLOAD_UPDATE, serverFile.getFileSize());
        } catch (Exception e) {
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            } else {
                Log.e(TAG, e.getMessage());
            }
        } finally {
            if (response != null) {
                response.close();
            }
        }
    }

    /**
     * OkHttp Request Body implementation to get progress updates for upload.
     * Retrieved from https://gist.github.com/eduardb/dd2dc530afd37108e1ac.
     */
    private static class CountingFileRequestBody extends RequestBody {

        /**
         * Okio buffer size
         */
        private static final int SEGMENT_SIZE = 60 * 1024; // okio.Segment.SIZE
        /**
         * File to be uploaded
         */
        private final File file;
        /**
         * Progress upload listener
         */
        private final ListenerProgress listener;
        /**
         * Content type
         */
        private final String contentType;

        /**
         * CountingFileRequestBody constructor
         *
         * @param file        file to be uploaded
         * @param contentType content-type
         * @param listener    progress listener
         */
        private CountingFileRequestBody(File file, String contentType, ListenerProgress listener) {

            this.file = file;
            this.contentType = contentType;
            this.listener = listener;
        }

        /**
         * Getter for file content length
         *
         * @return file content length
         */
        @Override
        public long contentLength() {

            return file.length();
        }

        /**
         * Mime type of data
         *
         * @return mime type of data
         */
        @Override
        public MediaType contentType() {

            return MediaType.parse(contentType);
        }

        /**
         * RequestBody reads the file content to a Okio sink
         *
         * @param sink Buffered sink to get file content
         * @throws IOException
         */
        @Override
        public void writeTo(BufferedSink sink) throws IOException {

            Source source = null;

            try {
                source = Okio.source(file);
                long read;

                while ((read = source.read(sink.buffer(), SEGMENT_SIZE)) != -1) {
                    sink.flush();
                    this.listener.transferred(read);
                }
            } finally {
                Util.closeQuietly(source);
            }
        }

        /**
         * Upload progress listener
         */
        public interface ListenerProgress {

            void transferred(long num);
        }
    }

    /**
     * ResponseBody implementation to intercept the response and call the progress listener
     */
    private static class ProgressResponseBody extends ResponseBody {
        /**
         * ResponseBody instance
         */
        private final ResponseBody responseBody;
        /**
         * Progress download listener
         */
        private final ProgressListener progressListener;
        /**
         * Buffer with content source
         */
        private BufferedSource bufferedSource;

        /**
         * ProgressResponseBody constructor
         *
         * @param responseBody     OkHttp ResponseBody received on Response
         * @param progressListener listener to update the progress of response reading
         */
        ProgressResponseBody(ResponseBody responseBody, ProgressListener progressListener) {

            this.responseBody = responseBody;
            this.progressListener = progressListener;
        }

        /**
         * Mime type of data
         *
         * @return mime type of data
         */
        @Override
        public MediaType contentType() {

            return responseBody.contentType();
        }

        /**
         * Getter for file content length
         *
         * @return file content length
         */
        @Override
        public long contentLength() {

            return responseBody.contentLength();
        }

        /**
         * Getter for the Buffered source
         *
         * @return Buffered source
         */
        @Override
        public BufferedSource source() {

            if (bufferedSource == null) {
                bufferedSource = Okio.buffer(source(responseBody.source()));
            }
            return bufferedSource;
        }

        /**
         * Forwards the Source ResponseBody and calls the Listener with the update progress.
         *
         * @param source Okio Source that reads the Response stream
         * @return Okio Source forwarded from input
         */
        private Source source(Source source) {

            return new ForwardingSource(source) {
                long totalBytesRead = 0L;

                @Override
                public long read(Buffer sink, long byteCount) throws IOException {
                    long bytesRead = super.read(sink, byteCount);

                    // read() returns the number of bytes read, or -1 if this source is exhausted.
                    totalBytesRead = bytesRead != -1 ? bytesRead : 0;
                    progressListener.update(totalBytesRead, responseBody.contentLength(),
                            bytesRead == -1);
                    return bytesRead;
                }
            };
        }
    }

    /**
     * Progress ListenerInterface to publish the progress update.
     */
    interface ProgressListener {

        void update(long bytesRead, long contentLength, boolean done);
    }

    /**
     * Updates the UI sending messages through the Handler.
     *
     * @param messageWhat Message 'what' code
     * @param bytesRead   number of bytes read
     */
    private void updateUI(int messageWhat, long bytesRead) {

        Message message = new Message();
        message.what = messageWhat;
        HashMap<String, Object> messageObject = new HashMap<>();

        //Updates the message what when needed
        switch (messageWhat) {
            case FragmentHome.DOWNLOAD_UPDATE: {
                messageObject.put(BYTES_READ, bytesRead);
                break;
            }
            case FragmentHome.UPLOAD_UPDATE: {
                messageObject.put(BYTES_READ, bytesRead);
                break;
            }
        }

        message.obj = messageObject;
        try {
            if (toActivity != null) {

                //send message
                toActivity.dispatchMessage(message);
            }
        } catch (Exception e) {
            if (BuildConfig.TRACE) {
                e.printStackTrace();
            } else {
                Log.e(TAG, e.getMessage());
            }
        }
    }
}
