/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain.utils;

import java.security.SecureRandom;
import java.util.Locale;
import java.util.Random;

/**
 * Creator of Random Strings to use as Trials identifiers.
 */
public class RandomString {

    /**
     * Upper case alphabet
     */
    public static final String UPPER = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
    /**
     * Lower case alphabet
     */
    public static final String LOWER = UPPER.toLowerCase(Locale.ROOT);
    /**
     * Numbers
     */
    public static final String DIGITS = "0123456789";
    /**
     * Alphanumeric string with values previous defined
     */
    public static final String ALPHANUM = UPPER + LOWER + DIGITS;
    /**
     * Random instance
     */
    private final Random random;
    /**
     * Array of char to keep symbols
     */
    private final char[] symbols;
    /**
     * Array of char to keep string generated
     */
    private final char[] buf;

    /**
     * RandomString constructor, creates a random string of the size given.
     *
     * @param length  length of String to be generated
     * @param random  instance of Random
     * @param symbols array of chars
     * @throws IllegalArgumentException case length is lower than 1 or symbols length is lower
     *                                  than 2
     */
    public RandomString(int length, Random random, String symbols) throws IllegalArgumentException {

        if (length < 1) throw new IllegalArgumentException();
        if (symbols.length() < 2) throw new IllegalArgumentException();
        this.random = random;
        this.symbols = symbols.toCharArray();
        this.buf = new char[length];
    }

    /**
     * Generate a random string.
     */
    public String nextString() {

        for (int idx = 0; idx < buf.length; ++idx) {
            buf[idx] = symbols[random.nextInt(symbols.length)];
        }
        return new String(buf);
    }

    /**
     * Create an alphanumeric string generator with specified length and using the specified
     * instance of Random
     *
     * @param length size of random to be generated
     * @param random instance of Random
     */
    public RandomString(int length, Random random) {

        this(length, random, ALPHANUM);
    }

    /**
     * Create an alphanumeric strings from a secure generator.
     *
     * @param length size of random to be generated
     */
    public RandomString(int length) {

        this(length, new SecureRandom());
    }

    /**
     * Create session identifiers.
     */
    public RandomString() {

        this(21);
    }

}