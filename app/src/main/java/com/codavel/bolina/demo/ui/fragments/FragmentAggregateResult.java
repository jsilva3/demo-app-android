/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.fragments;

import android.content.Context;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.codavel.bolina.demo.AppData;
import com.codavel.bolina.demo.R;
import com.codavel.bolina.demo.ui.adapters.TabAdapter;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Fragment to handle all the Aggregation fragments
 */
public class FragmentAggregateResult extends Fragment {
    /**
     * Application Context
     */
    private Context mContext;
    /**
     * Adapter for child Aggregation Fragments
     */
    private TabAdapter tabAdapter;
    /**
     * TAG used for logging
     */
    private static final String TAG = AppData._TAG + "[AGGREGATE-RES]";

    @BindView(R.id.tabViewPager)
    ViewPager tabViewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    /**
     * Inflates the layout on the View
     *
     * @param inflater           Layout inflater
     * @param container          ViewGroup container
     * @param savedInstanceState Bundle with saved instance
     * @return View
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_aggregate_results, container, false);
    }

    /**
     * When View is created we define the adapter for the ViewPager, setup the speedometer,
     * update server and network labels and setup the click listeners.
     *
     * @param view               View created
     * @param savedInstanceState Bundle with saved instance
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        mContext = getContext();
        ButterKnife.bind(this, view);

        tabAdapter = new TabAdapter(getFragmentManager());
        tabAdapter.addFragment(new FragmentAggNetwork(), getResources().getString(
                R.string.network));
        tabAdapter.addFragment(new FragmentAggServer(), getResources().getString(R.string.server));
        tabAdapter.addFragment(new FragmentAggBoth(), getResources().getString(R.string.both));

        for (int i = 0; i < tabAdapter.getCount(); i++) {
            tabAdapter.getItem(i);
        }

        tabViewPager.setAdapter(tabAdapter);
        tabLayout.setupWithViewPager(tabViewPager);

        highLightCurrentTab(0, mContext); // for initial selected tab view
        tabViewPager.addOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int
                    positionOffsetPixels) {
            }

            @Override
            public void onPageSelected(int position) {

                highLightCurrentTab(position, mContext);
            }

            @Override
            public void onPageScrollStateChanged(int state) {
            }
        });
    }

    /**
     * Set a hint to the system about whether this fragment's UI is currently visible to the user.
     *
     * @param isVisibleToUser boolean indicating if fragment is visible to the user
     */
    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {

        super.setUserVisibleHint(isVisibleToUser);
        if (isVisibleToUser) {
            try {
                getFragmentManager().beginTransaction().detach(this).attach(this).commit();
            } catch (Exception e) {
                Log.e(TAG, "Exception on FragResults transition");
            }
        }
    }

    /**
     * Highlights the tab in TabAdapter if he's the one selected.
     *
     * @param position Tab position selected
     * @param context  application context
     */
    private void highLightCurrentTab(int position, Context context) {

        for (int i = 0; i < tabLayout.getTabCount(); i++) {
            TabLayout.Tab tab = tabLayout.getTabAt(i);
            assert tab != null;
            tab.setCustomView(null);
            tab.setCustomView(tabAdapter.getTabView(i, context));
        }
        TabLayout.Tab tab = tabLayout.getTabAt(position);
        assert tab != null;
        tab.setCustomView(null);
        tab.setCustomView(tabAdapter.getSelectedTabView(position, context));
    }

}
