/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.adapters;

/**
 * Enum to identify the type of adapter
 */
public enum AdapterResultEnum {
    /**
     * Test is ended and results will be presented
     */
    TEST_RESULT,
    /**
     * Results persisted will be fetched and presented
     */
    HISTORY_RESULT,
    /**
     * Aggregate by Network
     */
    NETWORK_AGG,
    /**
     * Aggregate by Server
     */
    SERVER_AGG,
    /**
     * Aggregate Both
     */
    BOTH_AGG
}
