/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.fragments;

import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codavel.bolina.demo.BuildConfig;
import com.codavel.bolina.demo.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * About Fragment implementation
 */
public class FragmentAbout extends Fragment {

    @BindView(R.id.tvVersionName)
    TextView tvVersionName;

    /**
     * Inflates the about fragment
     *
     * @param inflater           Layout inflater
     * @param container          ViewGroup container for the layout
     * @param savedInstanceState Bundle with saved options
     * @return View created
     */
    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container,
                             @Nullable Bundle savedInstanceState) {

        return inflater.inflate(R.layout.fragment_about, container, false);
    }

    /**
     * When created the view is binded and a text label with about info is defined
     *
     * @param view               View created
     * @param savedInstanceState Bundle with saved options
     */
    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {

        super.onViewCreated(view, savedInstanceState);
        ButterKnife.bind(this, view);
        tvVersionName.setText("Version: " + BuildConfig.VERSION_NAME);
    }

}
