/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.ui.adapters;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.codavel.bolina.demo.R;
import com.codavel.bolina.demo.domain.utils.CommonObjects;
import com.codavel.bolina.demo.ui.interfaces.ListItem;
import com.codavel.bolina.demo.ui.interfaces.MyLinkedMap;
import com.codavel.bolina.demo.ui.interfaces.QuickResultHeader;
import com.codavel.bolina.demo.ui.interfaces.QuickResultItem;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Adapter to present Results to the user
 */
public class ResultAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    /**
     * Application Context
     */
    private Context mContext;
    /**
     * LinkedHashMap with list items
     */
    private MyLinkedMap<String, ListItem> arrayList;
    /**
     * Type of adapter
     */
    private AdapterResultEnum adaterType;

    /**
     * ResultAdapter constructor
     *
     * @param mContext   application context
     * @param arrayList  list of items to be presented
     * @param adaterType type of adapter
     */
    public ResultAdapter(Context mContext, MyLinkedMap<String, ListItem> arrayList,
                         AdapterResultEnum adaterType) {

        this.adaterType = adaterType;
        this.arrayList = arrayList;
        this.mContext = mContext;
    }

    /**
     * Returns a Header or Item view holder based on the ListItem type
     *
     * @param viewGroup view group
     * @param viewType  view type
     * @return Item or Header ViewHolder
     */
    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int viewType) {

        if (viewType == ListItem.TYPE_QR_HEADER)
            return new HeaderViewHolder(LayoutInflater.from(mContext).inflate(
                    R.layout.item_quick_result_header, viewGroup, false));
        else if (viewType == ListItem.TYPE_QR_ITEM)
            return new ItemViewHolder(LayoutInflater.from(mContext).inflate(
                    R.layout.item_quick_result, viewGroup, false));
        return null;
    }

    /**
     * Based on the ListItem type, fills the Header and Items lists with the values received
     *
     * @param viewHolder view holder
     * @param position   LinkedHashMap position
     */
    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder viewHolder, int position) {

        int type = arrayList.getValue(position).getType();

        if (type == ListItem.TYPE_QR_HEADER) {
            HeaderViewHolder headerViewHolder = (HeaderViewHolder) viewHolder;
            QuickResultHeader item = (QuickResultHeader) arrayList.getValue(position);
            switch (adaterType) {
                case BOTH_AGG: {
                    headerViewHolder.tvHeaderName.setText(item.testData.server);
                    headerViewHolder.tvSubType.setText(item.testData.network);
                    break;
                }
                case SERVER_AGG: {
                    headerViewHolder.tvHeaderName.setText(item.testData.server);
                    break;
                }
                case NETWORK_AGG: {
                    headerViewHolder.tvHeaderName.setText(item.testData.network);
                    break;
                }
                case TEST_RESULT:
                case HISTORY_RESULT: {
                    headerViewHolder.tvDate.setText(CommonObjects.getFormattedDate(
                            Long.parseLong(item.testData.date)));
                    headerViewHolder.tvTime.setText(DateUtils.formatDateTime(mContext, Long.parseLong(
                            item.testData.date), DateUtils.FORMAT_SHOW_TIME));
                    headerViewHolder.tvHeaderName.setText(item.testData.network);
                    headerViewHolder.tvSubType.setText(item.testData.server);
                    break;
                }
            }
        } else if (type == ListItem.TYPE_QR_ITEM) {
            ItemViewHolder holder = (ItemViewHolder)
                    viewHolder;
            QuickResultItem testData = null;
            try {
                testData = (QuickResultItem) arrayList.getValue(position);
                holder.tvDownload.setText(CommonObjects.getFormattedNumber(
                        testData.testData.downloadSpeed));
                holder.tvUpload.setText(CommonObjects.getFormattedNumber(
                        testData.testData.uploadSpeed));
                holder.tvPing.setText(CommonObjects.getFormattedNumber(testData.testData.ping));
                holder.tvProtocol.setText(testData.testData.protocol);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * Getter for the type of item in the list. It can be Header or Item.
     *
     * @param type ListItem type identifier
     * @return ListItem type
     */
    @Override
    public int getItemViewType(int type) {

        if (arrayList.getValue(type).getType() == ListItem.TYPE_QR_HEADER) {
            return ListItem.TYPE_QR_HEADER;
        } else {
            return ListItem.TYPE_QR_ITEM;
        }
    }

    /**
     * Getter for the number of entries
     *
     * @return number of entries
     */
    @Override
    public int getItemCount() {

        return arrayList.size();
    }

    /**
     * Header line view holder to separate trial results entries
     */
    class HeaderViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.tvDate)
        TextView tvDate;
        @BindView(R.id.tvTime)
        TextView tvTime;
        @BindView(R.id.tvNetwork)
        TextView tvHeaderName;
        @BindView(R.id.tvServer)
        TextView tvSubType;

        HeaderViewHolder(View inflate) {
            super(inflate);
            ButterKnife.bind(this, inflate);
        }
    }

    /**
     * Items entries of trial results entries
     */
    class ItemViewHolder extends RecyclerView.ViewHolder {
        @BindView(R.id.tvProtocol)
        TextView tvProtocol;
        @BindView(R.id.tvDownload)
        TextView tvDownload;
        @BindView(R.id.tvUpload)
        TextView tvUpload;
        @BindView(R.id.tvPing)
        TextView tvPing;

        ItemViewHolder(View inflate) {
            super(inflate);
            ButterKnife.bind(this, inflate);
        }
    }
}
