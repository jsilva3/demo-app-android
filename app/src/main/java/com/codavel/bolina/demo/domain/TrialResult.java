/****************************************************************************
 * Copyright © 2019 Codavel                                                 *
 *                                                                          *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may  *
 * not use this file except in compliance with the License. You may obtain  *
 * a copy of the License at:                                                *
 *                                                                          *
 *   http://www.apache.org/licenses/LICENSE-2.0                             *
 *                                                                          *
 * Unless required by applicable law or agreed to in writing, software      *
 * distributed under the License is distributed on an "AS IS" BASIS,        *
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. *
 * See the License for the specific language governing permissions and      *
 * limitations under the License.                                           *
 ****************************************************************************/

package com.codavel.bolina.demo.domain;

/**
 * Aggregates download and upload Trial as a result
 */
public class TrialResult {
    /**
     * Trial download
     */
    private Trial downloadTrial;
    /**
     * Trial upload
     */
    private Trial uploadTrial;

    /**
     * Identifier for `n` TrialResults, where `n` is the number of protocols available in the
     * TestProfile. (e.g. If I run a profile with Quick and Bolina, this id allow to identify which
     * TrialResults were compared). This is how we separate the results presentation
     */
    private String protocolCompareId;

    /**
     * Define the TrialResult trials checking if they are a download or upload Trial.
     * TrialResult is a pair of two trials performed using two different modes(upload and download).
     * Here we verify that condition, usually before persist in database.
     *
     * @param trial Trial to be added
     * @return true if Trial is validated and added, otherwise false
     */
    public boolean addTrial(Trial trial) {

        if (trial.getMode() == null || downloadTrial != null && uploadTrial != null) {
            return false;
        }

        switch (trial.getMode()) {
            case GET: {
                if (downloadTrial != null) {
                    return false;
                }
                downloadTrial = trial;
                break;
            }
            case POST: {
                if (uploadTrial != null) {
                    return false;
                }
                this.uploadTrial = trial;
                break;
            }
            default: {
                return false;
            }
        }
        return true;
    }

    /**
     * Getter for download Trial
     *
     * @return download Trial
     */
    public Trial getDownloadTrial() {

        return downloadTrial;
    }

    /**
     * Getter for upload Trial
     *
     * @return upload Trial
     */
    public Trial getUploadTrial() {

        return uploadTrial;
    }

    /**
     * Setter of protocol comparison identifier
     *
     * @param protocolCompareId protocol comparison identifier
     */
    public void setProtocolCompareId(String protocolCompareId) {

        this.protocolCompareId = protocolCompareId;
    }

    /**
     * Getter for protocol comparison identifier
     *
     * @return protocol comparison identifier
     */
    public String getProtocolCompareId() {

        return this.protocolCompareId;
    }

    /**
     * Calculates the average RTT returned from ping tests.
     *
     * @return average RTT
     */
    public double getAverageRTT() {

        if (downloadTrial.getAvgRTT() > 0 && uploadTrial.getAvgRTT() > 0) {
            return (downloadTrial.getAvgRTT() + uploadTrial.getAvgRTT()) / 2.0;
        } else if (downloadTrial.getAvgRTT() > 0) {
            return downloadTrial.getAvgRTT();
        } else if (uploadTrial.getAvgRTT() > 0) {
            return uploadTrial.getAvgRTT();
        } else {
            return 0;
        }
    }
}
