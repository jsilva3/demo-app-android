# Release Notes

## 0.9.1

Bolina interceptor: v0.9.1
Okhttp3:            v3.12.1

### Features

* **REMOTE CONFIG**
* **LOCAL CONFIG**
* **NETWORK CONDITIONS**

### Added
N/A
### Changed
N/A
### Fixed
N/A
### Known issues
