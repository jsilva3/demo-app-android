# Bolina Android Demo App

[![Platform](https://img.shields.io/badge/platform-android-lightgrey.svg?style=flat)](https://gitlab.com/codavel/demo-app-android)
[![Twitter](https://img.shields.io/badge/twitter-@Codavel-blue.svg?style=flat)](https://twitter.com/codavel)

This Android app was developed to test Bolina and [Bolina sample project](https://gitlab.com/codavel/bolina-sample-project).

Similar to Ookla speedtest app, this app tests the channel capacity using different protocols like (HTTPS, Bolina, Quic, etc...).
The results are aggregated within the app but also available in a Kibana dashboard (Check the [sample project documentation](https://www.codavel.com/sample-project-operating-system/) to learn more about it)
The current version of the App brings Bolina and HTTPS as the default testing protocols.

## Contents

- [Requirements](#requirements)
- [Installation](#installation)
- [Usage](#usage)   
  - [Configuration](#configuration)
- [FAQ](#faq)
- [Credits](#credits)
- [Social](#social)
- [License](#license)

## Getting started

### Requirements
---
* Git (version 2.13 or **higher**)
* Gradle (version 3.1.x or **higher**)
* Android Studio (version 3.x or **higher**)
* Android Device/Simulator (API 9.0 or **higher**)

### Installation
---

In order to start using the App there are a few steps that you need to follow:

1. The first one is to download the App project, by clone it:
    ```bash
    git clone git@gitlab.com:codavel/demo-app-android.git
    ```
1. The project is an Android Studio project, so finally, open the Android Studio
an then project you have cloned in previous step.

### Usage

#### Configuration
---
Configuring the app can be made using a local or a remote configuration.
The selection is made on the app `build.gradle`:
```c
/**
 * Configurations to run Bolina in the app
 */
defaultConfig {
    ...
    buildConfigField("boolean", "USE_LOCAL_CONFIG", '<boolean value>')
    ...
}
```
Changing the config value to `true` will use the local configuration (the file placed in assets like the following image shows).

![Config JSON file](doc/resources/config.png)

The configuration is a `json` file whose structure is presented in the `Configuration Structure` section.
Setting the value to false will read configurations from a remote source as explained in the sub section [Remote Configuration](#remote-configuration)  

##### Local Configuration

To use the local configuration, just make sure you have edited the previously explained `config.json` file,
and the application gradle file has the following configuration:

```c
/**
 * Configurations to use local or remote configuration
 */
defaultConfig {
    ...
    buildConfigField("boolean", "USE_LOCAL_CONFIG", 'true')
    ...
}
```

##### Remote Configuration

For remote configuration you need to have a `config.json` file in a remote server accessible through URL and a authentication token case needed.
Place the URL and Token in the app `build.gradle` file:
```c
/**
 * Configurations to use local or remote configuration
 */
defaultConfig {

    buildConfigField("boolean", "USE_LOCAL_CONFIG", 'false')
    buildConfigField("String", "AUTH_TOKEN", '"<Your auth token>"')
    buildConfigField("String", "REMOTE_CONFIG",'"<the URL defined>"')
    ...
}
```
The app will start by fetch this config rendering the information as a Test

##### Configuration Structure

The configuration should be named **config.json** like the one you can find in the project assets folder.


*  `repeat_times` - defines the number of times the full suite of tests will be repeated. The default value is 2.
*  `time_seconds` - the time in seconds that each individual test will take (an individual test is defined by mode (`GET`/`POST`), server, protocol (`HTTP`/`BOLINA`), network conditions (latency and packet loss rate)).The default value is 5.
*  `n_threads` - the number of concurrent threads that will make HTTP requests and consequently the maximum number of simultaneous requests during the test time. The default value is 5.
Each thread will fetch and remove the first file from a queue of files - see below the description of files - and perform the respective request;
If there is no file in the queue of files (i.e., if they all have been consumed by all threads), the thread repeats it’s last request.
*  `servers`:  
    *  `city` - the city where the server is located (for layout purposes only, can be replaced by any desired string)
    *  `country` - the country where the server is located (for layout purposes only, can be replaced by any desired string)
    *  `address` - the server address (ip or dns). This always needs to be user defined!
    *  `port` - the port where the content server is expecting requests. This needs to be user defined (the default value for the sample project is 8443).
*  `repeat_times` - defines the number of times the full suite of tests will be repeated. The default value is 2.
*  `n_threads` - the number of concurrent threads that will make HTTP requests and consequently the maximum number of simultaneous requests during the test time. The default value is 5.
Each thread will fetch and remove the first file from a queue of files - see below the description of files - and perform the respective request;
If there is no file in the queue of files (i.e., if they all have been consumed by all threads), the thread repeats it’s last request.
*  `time_seconds` - the time in seconds that each individual test will take (an individual test is defined by mode (`GET`/`POST`), server, protocol (`HTTP`/`BOLINA`), network conditions (latency and packet loss rate)).The default value is 5.
*  `mode` - the type of HTTP requests to be performed (`GET`, `POST` or `BOTH`). The default value is `BOTH`.
*  `protocols` - an array with the type of protocols that will be used to perform the requests (`HTTPS` or `BOLINA`). An `HTTPS` test will use the Alamofire SDK while the `BOLINA` test will use the Bolina SDK library.
*  `network_conditions` - an array of network conditions to be applied (via request to the network emulator) that allow to induce latency and packet loss
    *  `plr` - the amount of packet loss rate that will be induced (both incoming and outgoing links)
    *  `latency` - the Round Trip Time (RTT) that will be induced (half in the incoming link, half in the outgoing link)
*  `files` - defines the list of requests to be performed
    *  `download_files` - the list of files that will be used to perform requests (`GET` and `POST`)
        *  `file` - is the URL file path:
            * when using `GET` the server address plus this string should give the resource url
            * when using `POST` a random file will be created with the filename contained in this string
        *  `size` - the size of the file
            * when using `GET` should contains the size of the file to be downloaded
            * when using `POST` contains the size of the file we wish to be uploaded
        *  `n_times` - the number of times each file is present in the file queue
    *  `upload_path` - the URL file path for the post

### FAQ
---

### Credits
---
Bolina Android Demo App is owned and maintained by [Codavel](https://codavel.com). You can follow us on Twitter at [@codavel](https://twitter.com/codavel) for project updates and releases.

### Social
---
* [Facebook](https://www.facebook.com/codavel/)
* [Linkedin](https://www.linkedin.com/company/codavel/)
* [Blog](https://blog.codavel.com/)
* [Medium](https://medium.com/codavel-blog)

### License
---
Bolina SpeedTest App is released under the Apache license. See [LICENSE](https://gitlab.com/codavel/demo-app-android/blob/master/LICENSE) for more information.
